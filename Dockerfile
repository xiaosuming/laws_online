FROM python:3.8
ENV PYTHONBUFFERED 1
ARG branchTag=${branchTag}
ENV branchTag ${branchTag}


RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get -y install vim \
    && apt-get -y install libreoffice \
    && mkdir -p /app/laws_online/ \
    && cd /app/laws_online/ \
    && mkdir logs\
    && mkdir media \
    && rm -r /var/lib/apt/lists/*

ENV CONFIG_FILE laws_online.settings.uat.py
WORKDIR /app/laws_online/

COPY ./laws_online/requirements/base.txt /app/laws_online/

RUN pip install --upgrade pip -i https://pypi.doubanio.com/simple/
RUN pip install -i https://pypi.doubanio.com/simple/ --trusted-host mirrors.aliyun.com -r /app/laws_online/base.txt \
    && rm -rf ~./cache/pip \
    && rm -rf /tmp

COPY ./ /app/laws_online/
EXPOSE 8000

RUN echo "$branchTag"
RUN chmod +x ./run_migrate.sh
CMD ["/bin/sh", "-c", "./run_migrate.sh $branchTag"]