user=xinhuo
ip=192.168.1.128
text=$*
ssh $user@$ip 'docker stop laws_online && docker rm laws_online'
ssh $user@$ip "docker run --restart=always --name laws_online -d -e DJANGO_CONFIGURATION=uat -v /home/xinhuo/laws_online/upload:/app/laws_online/laws_online/upload -v /home/xinhuo/laws_online/static:/app/laws_online/laws_online/static -v /home/xinhuo/laws_online/logs:/app/laws_online/laws_online/logs -v /home/xinhuo/laws_online/fonts:/usr/share/fonts -p 8000:8000 registry.cn-shanghai.aliyuncs.com/xinhuodev/laws_online:$text"