# Generated by Django 3.1.5 on 2021-03-22 23:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0035_auto_20210304_1043'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='amount',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=9, verbose_name='平台收入'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='origin_amount',
            field=models.DecimalField(decimal_places=2, max_digits=9, verbose_name='账单金额'),
        ),
    ]
