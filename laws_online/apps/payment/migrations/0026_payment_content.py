# Generated by Django 3.1.5 on 2021-01-25 16:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0025_payment_deal_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='content',
            field=models.CharField(default='', max_length=200),
        ),
    ]
