from rest_framework import serializers
from .models import Payment, AliPayRecord, WechatPayRecord
from laws_online.apps.conversation.models import Conversation, ConversationType
from rest_framework.serializers import ValidationError


class AliPayRecordSerializer(serializers.ModelSerializer):
    buyer_logon_id = serializers.CharField(required=False)
    seller_email = serializers.CharField(required=False)

    class Meta:
        model = AliPayRecord
        fields = ['subject', 'gmt_payment', 'charset', 'seller_id', 'trade_status', 'buyer_id', 'auth_app_id',
                  'buyer_pay_amount', 'version', 'gmt_create', 'trade_no', 'fund_bill_list', 'app_id', 'notify_time',
                  'point_amount', 'total_amount', 'notify_type', 'out_trade_no', 'buyer_logon_id', 'notify_id',
                  'seller_email', 'receipt_amount', 'invoice_amount', 'sign']


class WechatPayRecordSerializer(serializers.ModelSerializer):
    settlement_total_fee = serializers.DecimalField(max_digits=9, decimal_places=2, required=False)
    fee_type = serializers.CharField(required=False)
    cash_fee_type = serializers.CharField(required=False)
    coupon_fee = serializers.DecimalField(max_digits=9, decimal_places=2, required=False)
    coupon_count = serializers.IntegerField(required=False)
    attach = serializers.CharField(required=False)
    device_info = serializers.CharField(required=False)
    sign_type = serializers.CharField(required=False)
    err_code = serializers.CharField(required=False)
    err_code_des = serializers.CharField(required=False)

    class Meta:
        model = WechatPayRecord
        fields = ['appid', 'mch_id', 'device_info', 'nonce_str', 'sign', 'sign_type', 'result_code',
                  'err_code', 'err_code_des', 'openid', 'is_subscribe', 'trade_type', 'bank_type', 'total_fee',
                  'settlement_total_fee', 'fee_type', 'cash_fee', 'cash_fee_type', 'coupon_fee', 'coupon_count',
                  'transaction_id', 'out_trade_no', 'attach', 'time_end', 'return_code', 'return_msg']


class PaymentSerializer(serializers.ModelSerializer):
    conversation = serializers.PrimaryKeyRelatedField(queryset=Conversation.objects.filter(is_deleted=False),
                                                      help_text='咨询id')
    origin_amount = serializers.DecimalField(max_digits=9, decimal_places=2, help_text='金额')
    conversation_type = serializers.PrimaryKeyRelatedField(queryset=ConversationType.objects.filter(is_deleted=False),
                                                           help_text='领域id')
    content = serializers.CharField(max_length=200, help_text='账单内容')
    out_trade_no = serializers.CharField(read_only=True)
    # content = serializers.CharField(max_length=200, help_text='服务内容')


    class Meta:
        model = Payment
        fields = ('id', 'conversation', 'from_user', 'to_user', 'conversation_type', 'origin_amount', 'out_trade_no',
                  'content', 'status')
        read_only_fields = ('id', 'from_user', 'to_user', 'conversation_type', 'out_trade_no', 'status')


class UpdatePaymentInfoSerializer(serializers.ModelSerializer):
    origin_amount = serializers.DecimalField(max_digits=9, decimal_places=2, help_text='金额')
    status = serializers.ChoiceField(choices=[3], help_text='更改订单状态，目前只能进行取消，也就是只能传3')
    content = serializers.CharField(max_length=200, help_text='账单内容')

    class Meta:
        model = Payment
        fields = ('origin_amount', 'status', 'content')



class UpdatePaymentSerializer(serializers.ModelSerializer):
    pay_type = serializers.ChoiceField(choices=[1, 2], help_text='支付方式，1为支付宝，2为微信', default=1)
    amount = serializers.DecimalField(max_digits=9, decimal_places=2, help_text='金额')

    class Meta:
        model = Payment
        fields = ('amount', 'pay_type')


class PaymentListSerialzier(serializers.ModelSerializer):
    out_trade_no = serializers.CharField()
    # refund_status = serializers.SerializerMethodField()
    conversation_type = serializers.CharField(source='conversation_type.name')
    amount = serializers.SerializerMethodField()
    # origin_amount = serializers.SerializerMethodField()
    #
    def get_amount(self, obj):
        return str(obj.origin_amount) if obj.status == 0 else str(obj.amount)
    #
    # def get_origin_amount(self, obj):
    #     return obj.origin_amount if obj.status != 0 else None



    class Meta:
        model = Payment
        fields = ('id', 'out_trade_no', 'create_time', 'status', 'amount', 'origin_amount', 'content', 'deal_time',
                  'conversation_type')


class LawyerPaymentSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(max_digits=9, decimal_places=2, help_text='金额')
    conversation_type_name = serializers.CharField(source='conversation_type.name')

    class Meta:
        model = Payment
        fields = ('id', 'amount', 'create_time', 'conversation_type_name', 'settlement_status', 'lawyer_commission')


class LawyerTotalPaymentSerialzier(serializers.Serializer):
    sum = serializers.DecimalField(max_digits=9, decimal_places=2)
    settlement_status = serializers.IntegerField()


# class UserSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = Users
#         fields = ['id', 'uid', 'nickname', 'is_active', 'phone', 'email', 'is_active']
#
# class LoginSerializer(serializers.ModelSerializer):
#     phone = serializers.IntegerField(help_text='臭傻逼',max_value=999)
#     password = serializers.IntegerField()
#
#     class Meta:
#         model = Users
#         fields = ['phone', 'password']

    # def create(self, validated_data):
