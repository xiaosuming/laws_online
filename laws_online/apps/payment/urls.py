from django.urls import path, include
from . import views
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'alipay/callback', views.AlipayCallBackView, 'alipay_callback')
router.register(r'info', views.PaymentView, 'payment_info')
router.register(r'wechat_pay/callback', views.WechatPayCallBackView, 'wechat_callback')
# router.register(r'family', views.GetFamilyInfoView, 'family_info')
# router.register(r'platform', views.GetPlatformDataView, 'platform')
# router.register(r'family_information', views.FamilyInformationView, 'family_public_information')


urlpatterns = [
    # path('payment/wechat_pay/callback', views.weixin_callback, name='wexin_callback'),
    path('payment/', include((router.urls, 'laws_online'), namespace='payment')),
]
