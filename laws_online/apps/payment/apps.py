from django.apps import AppConfig


class PaymentConfig(AppConfig):
    name = 'laws_online.apps.payment'
