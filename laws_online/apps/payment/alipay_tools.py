from alipay import AliPay,DCAliPay,ISVAliPay
from laws_online.settings.base import BASE_DIR
from pathlib import Path
import random
import datetime
import os

env = os.environ.get('DJANGO_SETTINGS_MODULE')

if env in ['laws_online.settings.master']:
    app_private_key_string = open(Path(BASE_DIR, 'apps/payment/alipay_keys/product/private_key.txt')).read()
    alipay_public_key_cert_string = open(Path(BASE_DIR,
                                              'apps/payment/alipay_keys/product/alipayCertPublicKey_RSA2.crt')).read()
    app_public_key_cert_string = open(
        Path(BASE_DIR, 'apps/payment/alipay_keys/product/appCertPublicKey_2021002141630454.crt')).read()
    alipay_root_cert_string = open(Path(BASE_DIR, 'apps/payment/alipay_keys/product/alipayRootCert.crt')).read()
    gate_way = 'https://openapi.alipay.com/gateway.do'
    app_id = '2021002141630454'
    notify_url = 'https://api.zfzxsh.com/api/v1/payment/alipay/callback'
    debug = False
else:
    app_private_key_string = open(Path(BASE_DIR, 'apps/payment/alipay_keys/dev/private_key.txt')).read()
    alipay_public_key_cert_string = open(Path(BASE_DIR,
                                              'apps/payment/alipay_keys/dev/alipayCertPublicKey_RSA2.crt')).read()
    app_public_key_cert_string = open(
        Path(BASE_DIR, 'apps/payment/alipay_keys/dev/appCertPublicKey_2021000117647021.crt')).read()
    alipay_root_cert_string = open(Path(BASE_DIR, 'apps/payment/alipay_keys/dev/alipayRootCert.crt')).read()
    gate_way = 'https://openapi.alipaydev.com/gateway.do'
    app_id = '2021000117647021'
    notify_url = 'http://47.101.137.78:30004/api/v1/payment/alipay/callback'
    debug = True

# app_private_key_string = open(Path(BASE_DIR, 'apps/payment/alipay_keys/private.txt')).read()
# alipay_public_key_string = open(Path(BASE_DIR, 'apps/payment/alipay_keys/public.txt')).read()


# ali_pay = AliPay(
#     appid="2016101000655027",
#     app_notify_url='http://47.101.137.78:30004/',  # 默认回调url
#     app_private_key_string=app_private_key_string,
#     # 支付宝的公钥，验证支付宝回传消息使用，不是你自己的公钥,
#     alipay_public_key_string=alipay_public_key_string,
#     sign_type="RSA2",  # RSA 或者 RSA2
#     debug=True,  # 默认False
# )

dc_alipay = DCAliPay(
    appid=app_id,
    app_notify_url=notify_url,
    app_private_key_string=app_private_key_string,
    app_public_key_cert_string=app_public_key_cert_string,
    alipay_public_key_cert_string=alipay_public_key_cert_string,
    alipay_root_cert_string=alipay_root_cert_string,
    debug=debug
)


subject = "iphone 12 max pro"

order_string = dc_alipay.api_alipay_trade_page_pay(
    out_trade_no=str(random.randint(1, 99999999)) + str(datetime.datetime.now()),
    total_amount=99,
    subject=subject,
    return_url=notify_url,  # 回调地址和默认配置一样即可 这里我选择百度
    notify_url=notify_url # 可选, 不填则使用默认notify url
)

app_order_string = dc_alipay.api_alipay_trade_app_pay(
    out_trade_no=str(random.randint(1, 99999999)) + str(datetime.datetime.now()),
    total_amount=1,
    subject=subject,
    return_url=notify_url,
    notify_url=notify_url,  # 可选, 不填则使用默认notify url
)

def get_app_order_string(out_trade_no, total_amount, subject, need_gate_way=False):
    app_order_string = dc_alipay.api_alipay_trade_app_pay(
        out_trade_no=out_trade_no,
        total_amount=total_amount,
        subject=subject,
        notify_url=notify_url,  # 可选, 不填则使用默认notify url
    )
    return f"{gate_way+'?' if need_gate_way else ''}{app_order_string}"

def api_alipay_fund_trans_toaccount_transfer(account, amount):
    result = dc_alipay.api_alipay_fund_trans_toaccount_transfer(
        out_biz_no=datetime.datetime.now().strftime("%Y%m%d%H%M%S"),
        payee_type="ALIPAY_LOGONID",
        payee_account=account,
        amount=amount,
        name="沙箱环境",
    )
    return result

def api_alipay_fund_trans_uni_transfer(account, amount):
    result = dc_alipay.api_alipay_fund_trans_uni_transfer(
        out_biz_no=datetime.datetime.now().strftime("%Y%m%d%H%M%S"),
        identity_type="ALIPAY_LOGON_ID",
        identity=account,
        trans_amount=amount,
        name="沙箱环境",
    )
    return result

def get_isv_alipay(code):
    return ISVAliPay(
        appid=app_id,
        app_notify_url=notify_url,
        debug=debug,
        app_auth_code=code,
        app_private_key_string=app_private_key_string,
        alipay_public_key_string=alipay_public_key_cert_string
    )

if __name__ == '__main__':
    # gate_way = 'https://openapi.alipaydev.com/gateway.do'
    print(f"{gate_way}?{order_string}")
    print(f"{app_order_string}")
    # account = "opdobi8062@sandbox.com"
    # print(api_alipay_fund_trans_toaccount_transfer(account, 9))
    # print(api_alipay_fund_trans_uni_transfer(account, 8))
    # dc_alipay.verified_sync_response()
    # a = AliPay()
    # a.verify()
