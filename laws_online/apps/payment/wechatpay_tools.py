from laws_online.settings.base import BASE_DIR
from weixin import Weixin, WeixinError
import os
from pathlib import Path

env = os.environ.get('DJANGO_SETTINGS_MODULE')

if env in ['laws_online.settings.master']:
    WEIXIN_MCH_KEY_FILE = Path(BASE_DIR, 'apps/payment/wechatpay_keys/apiclient_key.pem')
    WEIXIN_MCH_CERT_FILE = Path(BASE_DIR, 'apps/payment/wechatpay_keys/apiclient_cert.pem')
    APP_ID = 'wxf39a7480ed8a63be'
    MCH_ID = '1608897757'
    APP_SERCRET = 'b8e60fab71a374af19a11fc66c00fb12'
    APP_KEY = 'bi0gOEu7NWO1pSlNNyMg9mrJCkzPVzgl'

    # 微信统一下单URL
    WEIXIN_UNIFIED_ORDER_URL = 'https://api.mch.weixin.qq.com/pay/unifiedorder'
    # 微信查询订单URL
    WEIXIN_QUERY_ORDER_URL = 'https://api.mch.weixin.qq.com/pay/orderquery'
    NOTIFY_URL = 'https://api.zfzxsh.com/api/v1/payment/wechat_pay/callback'
    SIGN_TYPE = 'MD5'
    print(WEIXIN_MCH_CERT_FILE)
else:
    WEIXIN_MCH_KEY_FILE = ''
    WEIXIN_MCH_CERT_FILE = ''
    APP_ID = ''
    MCH_ID = ''
    APP_SERCRET = ''
    APP_KEY = ''

    # 微信统一下单URL
    WEIXIN_UNIFIED_ORDER_URL = ''
    # 微信查询订单URL
    WEIXIN_QUERY_ORDER_URL = ''
    NOTIFY_URL = ''
    SIGN_TYPE = 'MD5'

config = dict(
    WEIXIN_APP_ID=APP_ID,
    WEIXIN_APP_SECRET=APP_SERCRET,
    WEIXIN_MCH_ID=MCH_ID,
    WEIXIN_MCH_KEY=APP_KEY,
    WEIXIN_NOTIFY_URL=NOTIFY_URL,
    # 公钥文件
    WEIXIN_MCH_KEY_FILE=WEIXIN_MCH_KEY_FILE,
    # 私钥文件
    WEIXIN_MCH_CERT_FILE=WEIXIN_MCH_CERT_FILE
)

weixin = Weixin(config)

def app_pay_sign(out_trade_no, total_amount, body, weixin=weixin):
    jsdict = weixin.jsapi(
        #订单号
        out_trade_no=out_trade_no,
        #商品描述
        body=body,
        #支付金额
        total_fee=total_amount,
        #交易类型
        trade_type='APP'
    )
    jsdict["prepay_id"] = jsdict.get("package").split("=")[-1]
    try:
        data = {}
        data["appid"] = jsdict["appId"]
        data["partnerid"] = MCH_ID
        data["prepayid"] = jsdict["prepay_id"]
        data["noncestr"] = jsdict["nonceStr"]
        data["timestamp"] = jsdict["timeStamp"]
        data["package"] = jsdict["package"]
        jsdict['sign'] = weixin.sign(data)
        jsdict["partnerid"] = MCH_ID
        return jsdict
    except KeyError as e:
        print(e)
        return


def wechatpay_refund(out_trade_no, out_request_no, total_fee, refund_fee, weixin=weixin):
    try:
        return weixin.refund(
            out_trade_no=out_trade_no,
            out_refund_no=out_request_no,
            total_fee=total_fee,
            refund_fee=refund_fee
        )
    except WeixinError as e:
        return e

# def wechatpay_pay_individual(partner_trade_no, ):
#     try:
#         return weixin.pay_individual(
#             partner_trade_no=partner_trade_no,
#             out_refund_no=out_request_no,
#             total_fee=total_fee,
#             refund_fee=refund_fee
#         )
#     except WeixinError as e:
#         return e

if __name__ == '__main__':
    # r = app_pay_sign('12345678901', 1, '测试')
    # print(r)
    # refund = wechatpay_refund('4490828556394303489', '4490828556394303400', 1, 1)
    # print(refund)
    transfer = weixin.refund()
