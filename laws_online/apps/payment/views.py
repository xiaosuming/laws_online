from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin
from rest_framework import status
from rest_framework.permissions import AllowAny
from laws_online.libs.utils.tools import Response
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet, GenericViewSet, ReadOnlyModelViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, ListModelMixin
from rest_framework_xml.parsers import XMLParser
from .models import Payment, AliPayRecord, WechatPayRecord
from .alipay_tools import get_app_order_string, dc_alipay
from .wechatpay_tools import app_pay_sign, weixin
from .serializers import (AliPayRecordSerializer, PaymentSerializer, UpdatePaymentSerializer, PaymentListSerialzier,
                          LawyerPaymentSerializer, LawyerTotalPaymentSerialzier, UpdatePaymentInfoSerializer,
                          WechatPayRecordSerializer)
from django_filters.rest_framework import DjangoFilterBackend
from .filters import PaymentSearch, LawyerPaymentSearch
from laws_online.libs.utils.tools import get_snowflake, TextXMLParser
from laws_online.apps.users.utils import post_im_message
from django.db import transaction
from django.db.models import Sum
from django.core.cache import cache
from decimal import Decimal
import copy
import datetime
import time
import json


# Create your views here.

class AlipayCallBackView(GenericViewSet, CreateModelMixin):
    queryset = AliPayRecord.objects.filter(is_deleted=False)
    serializer_class = AliPayRecordSerializer
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        '''
        支付宝回调接口，请勿使用
        '''
        # print(request.data)
        # for i in range(5):
        #     time.sleep(0.2)
        #     print(request.data)
        # payment = Payment.objects.filter(out_trade_no=request.data['out_trade_no'], is_deleted=False).first()
        # data = {**request.data}
        # data.pop('sign_type') if data.get('sign_type') else 0
        # data = {**data, **{'payment': payment}}
        # a = AliPayRecord.objects.create(**data)
        # print(a)
        # payment = Payment.objects.filter(out_trade_no=request.data['out_trade_no'], is_deleted=0).first()
        # payment.content = json.dumps(request.data, ensure_ascii=False)
        # payment.save()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sign_data = serializer.validated_data
        signature = sign_data.pop('sign')
        success = dc_alipay.verify(sign_data, signature)

        if not success or serializer.validated_data['trade_status'] not in ("TRADE_SUCCESS", "TRADE_FINISHED" ):
            return Response('FAIL', status=status.HTTP_404_NOT_FOUND)

        print('success')
        payment = Payment.objects.filter(out_trade_no=serializer.validated_data['out_trade_no'], is_deleted=0).first()
        if not payment:
            return Response('FAIL', status=status.HTTP_400_BAD_REQUEST)


        serializer.validated_data['payment'] = payment
        alipay_record = serializer.create(serializer.validated_data)
        if alipay_record.trade_status == 'TRADE_SUCCESS':
            with cache.lock(f'payment_{payment.pk}', timeout=10, blocking_timeout=10):
                payment = Payment.objects.filter(out_trade_no=serializer.validated_data['out_trade_no'],
                                                 is_deleted=0).first()
                if payment.status !=0 or payment.pay_type != 0:
                    return Response('FAIL', status=status.HTTP_400_BAD_REQUEST)

                payment.status = 1
                payment.amount = alipay_record.total_amount
                platform_commission = Decimal('0.1') * payment.amount
                platform_commission = platform_commission if platform_commission < Decimal('500') else Decimal('500')
                payment.platform_commission = platform_commission
                payment.lawyer_commission = payment.amount - platform_commission
                payment.deal_time = datetime.datetime.now()
                payment.deal_date = datetime.date.today()
                payment.pay_type = 1
                payment.save()



                im_data = {
                    'type': 100,  # 暂时用100， 应该是101
                    'title': '已支付咨询订单',
                    'conversation': payment.conversation.pk,
                    'conversation_type': payment.conversation_type.name,
                    'amount': str(payment.amount),
                    'origin_amount': str(payment.origin_amount),
                    'payment_id': payment.id,
                    'status': payment.status,
                    'out_trade_no': str(payment.out_trade_no),
                    'content': payment.content,
                }
                post_im_message(payment.to_user.uid, payment.from_user.uid, im_data)
        return HttpResponse('success')


class WechatPayCallBackView(GenericViewSet, CreateModelMixin):
    queryset = WechatPayRecord.objects.filter(is_deleted=False)
    serializer_class = WechatPayRecordSerializer
    permission_classes = [AllowAny]
    parser_classes = [TextXMLParser]

    def create(self, request, *args, **kwargs):
        sign_data = copy.deepcopy(request.data)

        if not weixin.check(sign_data):
            return HttpResponse(weixin.reply("签名验证失败", False), status=status.HTTP_401_UNAUTHORIZED)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if serializer.validated_data['return_code'] != 'SUCCESS':
            return Response('FAIL', status=status.HTTP_405_METHOD_NOT_ALLOWED)

        payment = Payment.objects.filter(out_trade_no=serializer.validated_data['out_trade_no'], is_deleted=0,
                                         pay_type=0).first()
        if not payment:
            return Response('FAIL', status=status.HTTP_403_FORBIDDEN)

        serializer.validated_data['payment'] = payment
        wechat_record = serializer.create(serializer.validated_data)
        if wechat_record.return_code == 'SUCCESS':
            with cache.lock(f'payment_{payment.pk}', timeout=10, blocking_timeout=10):
                payment = Payment.objects.filter(out_trade_no=serializer.validated_data['out_trade_no'],
                                                 is_deleted=0).first()
                if payment.status != 0 or payment.pay_type != 0:
                    return Response('FAIL', status=status.HTTP_404_NOT_FOUND)

                payment.status = 1
                payment.amount = wechat_record.total_fee / Decimal('100')
                platform_commission = Decimal('0.1') * payment.amount
                platform_commission = platform_commission if platform_commission < Decimal('500') else Decimal('500')
                payment.platform_commission = platform_commission
                payment.lawyer_commission = payment.amount - platform_commission
                payment.deal_time = datetime.datetime.now()
                payment.deal_date = datetime.date.today()
                payment.pay_type = 2
                payment.save()

                im_data = {
                    'type': 100,  # 暂时用100， 应该是101
                    'title': '已支付咨询订单',
                    'conversation': payment.conversation.pk,
                    'conversation_type': payment.conversation_type.name,
                    'amount': str(payment.amount),
                    'origin_amount': str(payment.origin_amount),
                    'payment_id': payment.id,
                    'status': payment.status,
                    'out_trade_no': str(payment.out_trade_no),
                    'content': payment.content,
                }
                post_im_message(payment.to_user.uid, payment.from_user.uid, im_data)
        # return_data = f'''<xml>
        #                   <return_code><![CDATA[SUCCESS]]></return_code>
        #                   <return_msg><![CDATA[OK]]></return_msg>
        #                  </xml>'''


        return HttpResponse(weixin.reply("OK", True))


class PaymentView(GenericViewSet, CreateModelMixin, RetrieveModelMixin):
    serializer_class = PaymentSerializer
    queryset = Payment.objects.filter(is_deleted=False).order_by('-id')
    filter_backends = [DjangoFilterBackend]
    filter_class = PaymentSearch

    def retrieve(self, request, *args, **kwargs):
        '''
        根据id获取账单
        '''
        instance = self.get_object()
        if request.user not in [instance.from_user, instance.to_user]:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        serializer = PaymentListSerialzier(instance, context={'request': self.request})
        return Response(serializer.data)

    @transaction.atomic
    @action(methods=['PUT', 'PATCH'], detail=True, url_path='update', serializer_class=UpdatePaymentInfoSerializer)
    def update_payment(self, request, *args, **kwargs):
        '''
            根据账单id更新账单
        '''
        pk = kwargs['pk']
        with cache.lock(f'payment_{pk}', timeout=10, blocking_timeout=10):
            instance = self.get_object()
            serializer = self.get_serializer(instance=instance, data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            conversation = instance.conversation
            if instance.from_user != request.user:
                return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
            if conversation.status != 1:
                return Response('咨询未处于开启状态', status=status.HTTP_400_BAD_REQUEST)
            if instance.status != 0:
                return Response('账单未处于未支付状态', status=status.HTTP_400_BAD_REQUEST)
            s1 = transaction.savepoint()
            instance = serializer.update(instance, serializer.validated_data)

            conversation_type = conversation.type
            from_user = conversation.lawyer
            to_user = conversation.ordinary_user
            # create_data = {
            #     **serializer.validated_data,
            #     'out_trade_no': get_snowflake(),
            #     'conversation': conversation,
            #     'conversation_type': conversation_type,
            #     'from_user': from_user,
            #     'to_user': to_user
            # }


            payment = instance
            im_data = {
                'type': 100,
                'title': '更新咨询服务订单',
                'conversation': conversation.pk,
                'conversation_type': conversation_type.name,
                'origin_amount': str(payment.origin_amount),
                'amount': str(payment.amount),
                'payment_id': payment.pk,
                'status': payment.status,
                'out_trade_no': str(payment.out_trade_no),
                'content': payment.content,
                'role': request.user.role
            }
            if post_im_message(from_user.uid, to_user.uid, im_data):
                transaction.savepoint_commit(s1)
                return Response({'id': payment.pk}, status=status.HTTP_200_OK)

            transaction.savepoint_rollback(s1)
            return Response('更新成功', status=status.HTTP_200_OK)


    @action(methods=['GET'], detail=False, url_path='user', serializer_class=PaymentListSerialzier,
            filter_backends=[DjangoFilterBackend], filter_class=PaymentSearch)
    def get_user_payment_list(self, request, *args, **kwargs):
        '''
        筛选参数 conversation: 咨询id, page: 页码, page_size: 分页大小
        {'id': '(int)账单id', 'out_trade_no': '(str)账单号', 'create_time': '(str)生成时间', 'status': '(int)账单状态',
        'amount': '(str)实际支付金额','origin_amount': '(str)服务费用', 'content': '(str)服务内容',
        'deal_time': '(str)支付时间', 'conversation_type':'(str)领域内容'}
        '''
        user = request.user
        if user.role in [100, 200]:
            queryset = self.filter_queryset(self.get_queryset().filter(to_user=user))
        elif user.role == 300:
            queryset = self.filter_queryset(self.get_queryset().filter(from_user=user))
        else:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'result': serializer.data})

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        '''
        添加账单
        '''
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        conversation = serializer.validated_data['conversation']
        if conversation.status != 1:
            return Response('咨询未处于开启状态', status=status.HTTP_400_BAD_REQUEST)
        conversation_type = conversation.type
        from_user = conversation.lawyer
        to_user = conversation.ordinary_user
        if not from_user or not to_user:
            return Response('律师或用户不存在', status=status.HTTP_400_BAD_REQUEST)
        if from_user != request.user:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        create_data = {
            **serializer.validated_data,
            'out_trade_no': get_snowflake(),
            'conversation': conversation,
            'conversation_type': conversation_type,
            'from_user': from_user,
            'to_user': to_user
        }

        s1 = transaction.savepoint()

        payment = serializer.create(create_data)
        im_data = {
            'type': 100,
            'title': '咨询服务订单',
            'conversation': conversation.pk,
            'conversation_type': conversation_type.name,
            'origin_amount': str(payment.origin_amount),
            'payment_id': payment.pk,
            'status': payment.status,
            'out_trade_no': str(payment.out_trade_no),
            'content': payment.content,
            'role': request.user.role
        }
        if post_im_message(from_user.uid, to_user.uid, im_data):
            transaction.savepoint_commit(s1)
            return Response({'id': payment.pk}, status=status.HTTP_201_CREATED)

        transaction.savepoint_rollback(s1)
        return Response('创建失败', status=status.HTTP_400_BAD_REQUEST)

    @transaction.atomic
    @action(methods=['PUT'], detail=True, url_path='alipay_app_order_string', serializer_class=UpdatePaymentSerializer)
    def get_payment_app_order_string(self, request, pk, *args, **kwargs):
        '''
        获取支付url
        '''
        with cache.lock(f'payment_{pk}', timeout=10, blocking_timeout=10):
            instance = self.get_object()
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            # s1 = transaction.savepoint()

            # conversation = instance.conversation
            # conversation_type = instance.conversation_type
            # amount = serializer.validated_data['amount']

            if instance.to_user != request.user:
                return Response('权限错误', status=status.HTTP_403_FORBIDDEN)

            if instance.status != 0:
                return Response('账单未处于未支付状态', status=status.HTTP_400_BAD_REQUEST)

            conversation = instance.conversation
            if conversation.status != 1:
                return Response('订单未开启', status=status.HTTP_400_BAD_REQUEST)

            if serializer.validated_data['amount'] < instance.origin_amount:
                return Response('金额不能小于律师给出的金额', status=status.HTTP_400_BAD_REQUEST)

            # update_data = serializer.validated_data
            # update_data['status'] = 1
            # update_data['deal_time'] = datetime.date.today()
            # update_data['lawyer_commission'] = amount * Decimal('0.9') if amount * Decimal('0.9') < Decimal('500') else Decimal('500')
            # update_data['platform_commission'] = amount - update_data['lawyer_commission']
            # print(111)
            # serializer.update(instance, update_data)
            # print(222)
            pay_type = serializer.validated_data['pay_type']
            if pay_type == 1:
                return Response({'url': get_app_order_string(str(instance.out_trade_no),
                                                             str(serializer.validated_data['amount']),
                                                             '咨询服务订单')})
            else:
                return Response({'url': app_pay_sign(str(instance.out_trade_no),
                                                             int(serializer.validated_data['amount'] * Decimal('100')),
                                                             '咨询服务订单')})


    @action(methods=['GET'], detail=False, url_path='lawyer', serializer_class=LawyerPaymentSerializer,
            filter_class=LawyerPaymentSearch)
    def get_lawyer_payment_list(self, request, *args, **kwargs):
        '''
        律师获取账单列表, 当前时间的筛选请输入id={非常大的整数}或者不传，其后传id=last_id， 可传page_size,默认为10
        '''
        if request.user.role != 300:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)

        page_size = request.query_params.get('page_size', '10')
        if not page_size.isdigit():
            return Response('参数错误', status=status.HTTP_400_BAD_REQUEST)
        page_size = int(page_size) if int(page_size) < 100 else 99

        queryset = self.filter_queryset(self.get_queryset().filter(status=1,
                                        from_user=request.user).order_by('-create_time'))[0: page_size]
        serializer = self.get_serializer(queryset, many=True)
        data = {}

        for i, v in enumerate(serializer.data):
            create_time = datetime.datetime.strptime(v['create_time'], "%Y-%m-%d %H:%M:%S")
            year_month = f'{create_time.year}-{create_time.month}'
            if year_month in data.keys():
                data[year_month].append(v)
            else:
                data[year_month] = []
                data[year_month].append(v)

        data = [{'date': k, 'list': v} for k, v in data.items()]
        from django.db import connection
        for i in connection.queries:
            print(i)
        return Response({'result': data, 'last_id': serializer.data[-1]['id'] if serializer.data else None})

    @action(methods=['GET'], detail=False, url_path='lawyer_total', serializer_class=LawyerTotalPaymentSerialzier,
            filter_class=LawyerPaymentSearch)
    def get_lawyer_payment_total(self, request, *args, **kwargs):
        '''
        律师获取总计订单详情
        '''
        if request.user.role != 300:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)

        queryset = self.get_queryset().filter(status=1,
                    from_user=request.user).values('settlement_status').annotate(sum=Sum('amount')).order_by(
                    'settlement_status')
        print(queryset)
        result = {i: Decimal('0.00') for i in [0, 1]}
        for i in queryset:
            result[i['settlement_status']] = i['sum']

        result['all'] = sum([v for k, v in result.items()])
        result['settled'] = result[1]
        result['no_settled'] = result[0]
        result.pop(1)
        result.pop(0)
        # from django.db import connection
        # for i in connection.queries:
        #     print(i)
        # serializer = self.get_serializer(queryset, many=True)
        return Response({'result': {k: str(v) for k, v in result.items()}})


# def weixin_callback(request):
#     # print(request.data)
#     print(request.body)
#     print(request.body.decode())
#     print(request.body)
#     print(request.body.decode())
#     print(435, request.body)
#     print(436, request.body.decode())
#     return HttpResponse('', status=status.HTTP_404_NOT_FOUND)






            # 暂时把发送消息写在一起，此处应该写在支付宝的回调里面
            # im_data = {
            #     'type': 101,
            #     'title': '已支付咨询订单',
            #     'conversation': conversation.pk,
            #     'conversation_type': conversation_type.name,
            #     'amount': str(serializer.validated_data['amount']),
            #     'payment_id': instance.id,
            #     'status': instance.status,
            #     'out_trade_no': str(instance.out_trade_no),
            #     'content': conversation.content,
            #     'role': request.user.role
            # }
            # if post_im_message(instance.from_user.uid, instance.to_user.uid, im_data):
            #     transaction.savepoint_commit(s1)
            #     return Response({'url': get_app_order_string(str(instance.out_trade_no),
            #                                                  str(serializer.validated_data['amount']),
            #                                                  '咨询服务订单')})

            # transaction.savepoint_rollback(s1)
            # return Response('获取失败', status=status.HTTP_400_BAD_REQUEST)


# class UserView(GenericViewSet, RetrieveModelMixin):
#     serializer_class = UserSerializer
#     queryset = Users.objects.filter(is_deleted=0)
#
#     def retrieve(self, request, *args, **kwargs):
#         '''
#         :param request:
#         :param args:
#         :param kwargs:
#         :return: 获取个人详情
#         '''
#         instance = self.get_object()
#         serializer = self.get_serializer(instance)
#         return Response(serializer.data)
#
#     # @action(methods=['POST'], detail=False, url_path='test',serializer_class=LoginSerializer)
#     # def test(self, request, *args, **kwargs):
#     #     serializer = self.get_serializer(data=request.data)
#     #     serializer.is_valid(raise_exception=True)
#     #     return Response(1)
#
#     @action(methods=['POST'], detail=False, url_path='test')
#     def test(self, request, *args, **kwargs):
#         return Response(1)