from rest_framework import serializers
from .models import (Users, Lawyer, LawyerFile, LawyerConversationType, EnterPriseFile, EnterpriseUser, LawyerCase,
                     UserCollectionLawyer, LawyerAuthenticationInfoTemp, EnterpriseUserTemp, EnterPriseFileTemp,
                     LawyerFileTemp, LawyerConversationTypeTemp)
# from laws_online.apps.conversation.models import ConversationType
from rest_framework.serializers import ValidationError
from django.contrib.auth.hashers import make_password, check_password
from laws_online.apps.conversation.models import ConversationType
from laws_online.apps.city.models import City
from django.contrib.auth.models import AnonymousUser
from django.core.cache import cache
from django.db.models import Q
import json
# import requests
from .utils import add_im_account, update_im_account_info, set_im_info, remove_im_account
from laws_online.apps.payment.alipay_tools import app_id
from django.db import transaction
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework import exceptions
from django.utils.translation import ugettext as _
from rest_framework_jwt.compat import PasswordField
from django.contrib.auth import authenticate
from django.utils import timezone
import random
# from laws_online.libs.middlewares.UsernameMobileAuthBackend import authenticate
from rest_framework_jwt.settings import api_settings

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER


class UserSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(help_text='手机号', min_length=9)
    password = serializers.CharField(write_only=True, min_length=4, max_length=16, help_text='密码')
    role = serializers.ChoiceField(choices=[100, 200, 300], help_text="{100: '普通用户', 200: '公司用户', 300: '律师'}")
    uid = serializers.CharField(read_only=True)
    nickname = serializers.CharField(read_only=True)
    is_active = serializers.CharField(read_only=True)
    email = serializers.CharField(read_only=True)
    picture = serializers.ImageField(required=False, help_text='个人头像')
    is_authentication = serializers.SerializerMethodField(read_only=True)
    is_online = serializers.BooleanField(read_only=True)
    verification_code = serializers.CharField(help_text='验证码', write_only=True)
    status = serializers.CharField(read_only=True)

    def validate(self, attrs):
        verification_code = attrs.pop('verification_code')
        cache_key = f'phone_{attrs["phone"]}_{1}'
        cache_verification_code = cache.get(cache_key)
        if verification_code != cache_verification_code:
            raise ValidationError('验证码错误')
        return attrs

    def get_is_authentication(self, obj):
        return obj.is_authentication if obj.is_authentication not in [4, 5] else 2

    class Meta:
        model = Users
        fields = ['id', 'uid', 'nickname', 'is_active', 'phone', 'email', 'role', 'password', 'picture',
                  'is_authentication', 'is_online', 'status', 'verification_code']

    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data['password'])
        request = validated_data.pop('request')

        with transaction.atomic():
            # 创建事务保存点
            save_id = transaction.savepoint()

            user = Users.objects.create(**validated_data)
            user.nickname = user.uid
            if user.role == 100:
                user.is_authentication = 2
            user.save()
            picture = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user.picture.url}" if user.picture else None
            if add_im_account(user.uid, user.nickname, picture) and set_im_info(user.uid, role=user.role):
                # 提交从保存点到当前状态的所有数据库事务操作
                transaction.savepoint_commit(save_id)

                return user

            # 回滚到保存点
            transaction.savepoint_rollback(save_id)
        return None


class UserChangeRoleSerializer(serializers.ModelSerializer):
    role = serializers.ChoiceField(choices=[100, 200, 300], help_text="{100: '普通用户', 200: '公司用户', 300: '律师'}")
    nickname = serializers.CharField(required=False, help_text='昵称')
    # picture = serializers.ImageField(required=False, help_text='个人头像')
    verification_code = serializers.CharField(help_text='验证码', required=False)

    def validate(self, attrs):
        # verification_code = attrs.pop('verification_code')
        # user = self.context.get('request').user
        # cache_key = f'user_{user.id}_{user.phone}_{5}'
        # cache_verification_code = cache.get(cache_key)
        # if verification_code != cache_verification_code:
        #     raise ValidationError('验证码错误')
        return attrs

    class Meta:
        model = Users
        fields = ['nickname', 'role', 'verification_code']

    def create(self, validated_data):
        # validated_data['password'] = make_password(validated_data['password'])
        request = self.context.get('request')
        old_user = request.user
        new_role = validated_data['role']

        new_user = Users.objects.filter(is_deleted=False, role=new_role, phone=old_user.phone).first()

        if new_user:
            return new_user

        with transaction.atomic():
            # 创建事务保存点
            save_id = transaction.savepoint()

            # old_user.is_deleted = True
            # old_user.save()
            new_nickname = validated_data.get('nickname')
            old_user_nickname = old_user.nickname if new_nickname is None else new_nickname
            old_user_uid = old_user.uid
            user = old_user
            user.pk = None
            user.uid = None
            user.is_deleted = False
            user.nickname = old_user_nickname
            user.date_joined = timezone.now()
            user.role = validated_data['role']
            user.is_authentication = 2 if validated_data['role'] == 100 else 0
            user.save()

            if old_user_nickname == old_user_uid:
                user.nickname = user.uid
                user.save()
            # validated_data['picture'] = old_user.picture
            # validated_data['password'] = old_user.password
            # user = Users.objects.create(**validated_data)

            # old_user.save()
            # user.save()
            picture = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user.picture.url}" if user.picture else None
            if add_im_account(user.uid, user.nickname, picture) and set_im_info(user.uid, role=user.role):
                # 提交从保存点到当前状态的所有数据库事务操作

                # remove_im_account([old_user.uid])

                transaction.savepoint_commit(save_id)
                print(user)
                return user

            # 回滚到保存点
            transaction.savepoint_rollback(save_id)
        return None

    def get_jwt_token(self, new_user):
        payload = jwt_payload_handler(new_user)

        return {
            'token': jwt_encode_handler(payload),
            'user': new_user
        }


class UpdateUserInfoSerializer(serializers.ModelSerializer):
    nickname = serializers.CharField(help_text='昵称')
    picture = serializers.ImageField(help_text='个人头像')
    gender = serializers.IntegerField(help_text='性别')
    old_password = serializers.CharField(write_only=True, help_text='旧密码')
    new_password = serializers.CharField(write_only=True, min_length=4, max_length=16, help_text='新密码')
    phone = serializers.CharField(help_text='手机号', min_length=9)
    verification_code = serializers.CharField(help_text='验证码', write_only=True)
    is_new = serializers.BooleanField(help_text='新手引导的标记', required=False)

    def validate(self, attrs):
        if attrs.get('new_password') is not None and attrs.get('old_password') is None:
            raise ValidationError('缺少参数')
        return attrs

    def update(self, instance, validated_data):
        verification_code = validated_data.pop('verification_code', None)
        old_password = validated_data.pop('old_password', None)
        new_password = validated_data.pop('new_password', None)
        picture = validated_data.get('picture')
        phone = validated_data.get('phone')
        user = self.context.get('request').user

        picture = None if user.role == 300 else picture

        cache_key = f'user_{user.pk}_{phone if phone is not None else instance.phone}_{4}'
        confirm_cache_key = f'user_{user.id}_{instance.phone}_{5}'
        cache_verification_code = cache.get(cache_key)
        if new_password:
            # if verification_code is None or verification_code != cache_verification_code:
            #     raise ValidationError('验证码错误')
            if not check_password(old_password, instance.password):
                raise ValidationError('密码错误')
            if old_password == new_password and check_password(new_password, instance.password):
                raise ValidationError('新密码不能与旧密码相同')
            validated_data['password'] = make_password(new_password)
            Users.objects.filter(unionid=user.unionid, is_deleted=False).update(phone=phone)
            # cache.set(cache_key, '', 0)

        if phone:
            if (verification_code is None or verification_code != cache_verification_code or
                    cache.get(confirm_cache_key) != instance.phone):
                raise ValidationError('验证码错误')
            if Users.objects.filter(is_deleted=False, phone=phone).filter(~Q(pk=user.pk)).exists():
                raise ValidationError('手机号已被注册')
            cache.set(cache_key, '', 0)
            cache.set(cache_verification_code, '', 0)

            Users.objects.filter(unionid=user.unionid, is_deleted=False).update(phone=phone)

        instance = super().update(instance, validated_data)

        if picture:
            request = self.context.get('request')
            picture = (f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user.picture.url}"
                       if user.picture else None)
            set_im_info(instance.uid, image=picture)

        if validated_data.get('nickname'):
            set_im_info(instance.uid, nickname=validated_data.get('nickname'))

        return instance

    class Meta:
        model = Users
        fields = ['nickname', 'picture', 'gender', 'old_password', 'new_password', 'phone', 'verification_code',
                  'is_new']


# class LoginSerializer(serializers.Serializer):
#     uid = serializers.IntegerField(help_text='手机号')
#     verification_code = serializers.IntegerField(required=False)
#     password = serializers.IntegerField()


class MyJSONWebTokenSerializer(JSONWebTokenSerializer):
    """
    Serializer class used to validate a username and password.

    'username' is identified by the custom UserModel.USERNAME_FIELD.

    Returns a JSON Web Token that can be used to authenticate later calls.
    """

    def __init__(self, *args, **kwargs):
        """
        Dynamically add the USERNAME_FIELD to self.fields.
        """
        super(JSONWebTokenSerializer, self).__init__(*args, **kwargs)

        self.fields[self.username_field] = serializers.CharField(help_text='手机号')
        self.fields['password'] = PasswordField(write_only=True, required=False, help_text='密码，密码或验证码必须传入一个')
        self.fields['verification_code'] = serializers.CharField(required=False, help_text='验证码')
        self.fields['role'] = serializers.CharField(default=0, help_text='验证码')

    # @property
    # def username_field(self):
    #     return get_username_field()

    def validate(self, attrs):
        last_login_role = attrs.get('role')
        if not last_login_role:
            login_users = Users.objects.filter(phone=attrs.get(self.username_field),
                                               is_deleted=False).order_by('role')[:3].values('role', 'last_login')
            login_users_dict = {i['last_login']: i['role'] for i in login_users if i['last_login'] is not None}
            if login_users_dict:
                last_login_role = login_users_dict.get(max(login_users_dict), 100)
            else:
                last_login_role = login_users[0]['role'] if login_users else 100

        credentials = {
            self.username_field: attrs.get(self.username_field),
            'password': attrs.get('password'),
            'role': last_login_role
        }
        verification_code = {'verification_code': attrs.get('verification_code')}
        verification_code_credentials = {**credentials, **verification_code}

        random_str = list(range(97, 123)) + list(range(65, 91)) + list(range(48, 58))

        if all(credentials.values()):
            user = authenticate(**credentials)
            print(credentials)

            if user:
                if user.is_deleted != 0:
                    msg = _('用户不存在')
                    raise exceptions.AuthenticationFailed(msg)

                if not user.is_active:
                    msg = _('用户被禁用')
                    raise serializers.ValidationError(msg)

                # if user.status != 0:
                #     msg = _('用户被禁用，请联系客服')
                #     raise serializers.ValidationError(msg)

                payload = jwt_payload_handler(user)

                return {
                    'token': jwt_encode_handler(payload),
                    'user': user
                }
            else:
                msg = _('账号密码错误')
                raise serializers.ValidationError(msg)

        elif attrs.get(self.username_field) and verification_code:
            print(verification_code_credentials)

            # msg = _('验证码错误')
            # raise serializers.ValidationError(msg)

            cache_key = f'phone_{attrs.get(self.username_field)}_{1}'
            cache_verification_code = cache.get(cache_key)
            if attrs.get('verification_code') != cache_verification_code:
                msg = _('验证码错误')
                raise serializers.ValidationError(msg)

            user = authenticate(**verification_code_credentials)

            if user:
                if user.is_deleted:

                    random_password = ''.join([chr(random.choice(random_str)) for i in range(8)])

                    register_data = {
                        'phone': attrs.get(self.username_field),
                        'password': random_password,
                        'role': 100
                    }

                    new_user = self.create(register_data)

                    if new_user:
                        payload = jwt_payload_handler(new_user)

                        cache.set(cache_key, '', 0)

                        return {
                            'token': jwt_encode_handler(payload),
                            'user': new_user
                        }

                    msg = _('登录注册失败')
                    raise serializers.ValidationError(msg)
                    # msg = _('用户不存在')
                    # raise exceptions.AuthenticationFailed(msg)

                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)

                # if user.status != 0:
                #     msg = _('用户被禁用，请联系客服')
                #     raise serializers.ValidationError(msg)

                payload = jwt_payload_handler(user)

                cache.set(cache_key, '', 0)

                return {
                    'token': jwt_encode_handler(payload),
                    'user': user
                }
            else:

                random_password = ''.join([chr(random.choice(random_str)) for i in range(8)])

                register_data = {
                    'phone': attrs.get(self.username_field),
                    'password': random_password,
                    'role': 100
                }

                new_user = self.create(register_data)

                if new_user:
                    payload = jwt_payload_handler(new_user)

                    cache.set(cache_key, '', 0)

                    return {
                        'token': jwt_encode_handler(payload),
                        'user': new_user
                    }

                msg = _('登录注册失败')
                raise serializers.ValidationError(msg)

        else:
            msg = _('Must include "{username_field}" and "password".')
            msg = msg.format(username_field=self.username_field)
            raise serializers.ValidationError(msg)

    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data['password'])
        request = self.context.get('request')

        with transaction.atomic():
            # 创建事务保存点
            save_id = transaction.savepoint()

            user = Users.objects.create(**validated_data)
            user.nickname = user.uid
            if user.role == 100:
                user.is_authentication = 2
            user.save()
            picture = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user.picture.url}" if user.picture else None
            if add_im_account(user.uid, user.nickname, picture) and set_im_info(user.uid, role=user.role):
                # 提交从保存点到当前状态的所有数据库事务操作
                transaction.savepoint_commit(save_id)

                return user

            # 回滚到保存点
            transaction.savepoint_rollback(save_id)
        return None
    # class Meta:
    #     model = Users
    #     fields = ['phone', 'password']

    # def create(self, validated_data):


class LawyerFileSerializer(serializers.ModelSerializer):
    type = serializers.ChoiceField(choices=[1, 2, 3, 4], help_text='1:职业证图片,2:年审页图片,3:身份证图片,4:营业执照图片')
    file = serializers.ImageField(help_text='图片')

    class Meta:
        model = LawyerFile
        fields = ('type', 'file', 'id')


class LawyerSerializer(serializers.Serializer):
    type = serializers.ChoiceField(default=0, choices=[0, 1], help_text='0表示个人，1表示律所')
    gender = serializers.IntegerField(help_text='性别')
    ID_number = serializers.CharField(help_text='身份证')
    certificate_number = serializers.CharField(help_text='执照证号')
    license_period = serializers.DateField(help_text='执业年限')
    city = serializers.IntegerField(help_text='城市', default=310100)
    work_address = serializers.CharField(help_text='办公地址')
    conversation_types = serializers.ListField(child=serializers.CharField(), help_text='领域类型，类型')
    wechat_id = serializers.CharField(help_text='微信号')
    alipay_id = serializers.CharField(help_text='支付宝账号')
    picture = serializers.ImageField(help_text='律师头像', allow_null=True)
    profile = serializers.CharField(help_text='详细信息')
    nickname = serializers.CharField(help_text='名字')
    law_firm = serializers.CharField(help_text='律所', required=False)
    type_1 = serializers.ListField(child=serializers.ImageField(), help_text='职业证图片')
    type_2 = serializers.ListField(child=serializers.ImageField(), help_text='年审页图片')
    type_3 = serializers.ListField(child=serializers.ImageField(), help_text='身份证图片')
    type_4 = serializers.ListField(child=serializers.ImageField(), help_text='营业执照图片', required=False)

    # files = LawyerFileSerializer(many=True, help_text='该字段为数组,内层为对象,字段1，type（1：职业证，2：年审页，3：身份证， 4：营业执照），字段2, file(文件)')
    # types
    # def validate(self, attrs):
    #     if not attrs.get('files'):
    #         raise ValidationError('缺少附件参数')
    #     return attrs

    def create(self, validated_data):
        data = validated_data
        user = data['user']
        conversations_list = json.loads(data.pop('conversation_types')[0])
        type_1 = [{'type': 1, 'file': i, 'lawyer': user} for i in data.pop('type_1')]
        type_2 = [{'type': 2, 'file': i, 'lawyer': user} for i in data.pop('type_2')]
        type_3 = [{'type': 3, 'file': i, 'lawyer': user} for i in data.pop('type_3')]
        type_4 = data.pop('type_4', [])
        type_4 = [{'type': 4, 'file': i, 'lawyer': user} for i in type_4]
        request = data.pop('request')
        picture = data.pop('picture')
        nickname = data.pop('nickname')
        gender = data.pop('gender')
        profile = data.pop('profile')
        print(type(conversations_list), conversations_list)
        conversations = ConversationType.objects.filter(is_deleted=0, pk__in=conversations_list)
        if len(conversations) != len(conversations_list):
            raise ValidationError('领域类型错误')

        conversations_model = [LawyerConversationType(**{"lawyer": user, "conversation_type": i}) for i in
                               conversations]
        files_model = [LawyerFile(**i) for i in type_1 + type_2 + type_3 + type_4]
        # data['conversation_type'] = json.dumps([i.pk for i in conversations], ensure_ascii=False)

        # 设置事务保存点
        s1 = transaction.savepoint()
        # Users.objects.filter(pk=user.pk, is_deleted=False).update(picture=picture, nickname=nickname, gender=gender,
        #                                                           is_authentication=1, profile=profile)
        user.picture = picture
        user.nickname = nickname
        user.gender = gender
        user.is_authentication = 1
        user.profile = profile
        user.authentication_time = timezone.now()
        user.save()
        LawyerConversationType.objects.bulk_create(conversations_model)
        LawyerFile.objects.bulk_create(files_model)
        Lawyer.objects.create(**data)

        picture_url = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user.picture.url}"
        nickname = user.nickname
        if not update_im_account_info(user.uid, nickname=nickname, picture=picture_url):
            transaction.savepoint_rollback(s1)
            return False
        # 提交事务 (如果没有异常,就提交事务)
        transaction.savepoint_commit(s1)
        return True

    def update(self, instance, validated_data):
        data = validated_data
        user = data['user']
        conversations_list = json.loads(data.pop('conversation_types', [[]])[0])
        type_1 = [{'type': 1, 'file': i, 'lawyer': user} for i in data.pop('type_1', [])]
        type_2 = [{'type': 2, 'file': i, 'lawyer': user} for i in data.pop('type_2', [])]
        type_3 = [{'type': 3, 'file': i, 'lawyer': user} for i in data.pop('type_3', [])]
        type_4 = [{'type': 4, 'file': i, 'lawyer': user} for i in data.pop('type_4', [])]
        request = data.pop('request', None)
        picture = data.pop('picture', None)
        nickname = data.pop('nickname', None)
        gender = data.pop('gender', None)
        profile = data.pop('profile', None)

        s1 = transaction.savepoint()  # 设置事务保存点

        print(type(conversations_list), conversations_list)
        if conversations_list:
            conversations = ConversationType.objects.filter(is_deleted=0, pk__in=conversations_list)
            if len(conversations) != len(conversations_list):
                raise ValidationError('领域类型错误')
            conversations_model = [LawyerConversationType(**{"lawyer": user, "conversation_type": i}) for i in
                                   conversations]
            # 先删除原先的关系，再重新创新新的关系
            LawyerConversationType.objects.filter(lawyer=user, is_deleted=False).update(is_deleted=True)
            LawyerConversationType.objects.bulk_create(conversations_model)

        files_model = [LawyerFile(**i) for i in type_1 + type_2 + type_3 + type_4]

        if files_model:
            # LawyerFile.objects.filter(is_deleted=False, lawyer=user).update()
            LawyerFile.objects.bulk_create(files_model)

        if any([picture, nickname, gender, profile]):
            user.picture = picture
            user.nickname = nickname
            user.gender = gender
            user.is_authentication = user.is_authentication if user.is_authentication == 1 else 4
            user.profile = profile
            user.save()

        if data:
            for attr, value in data.items():
                setattr(instance, attr, value)

            instance.save()

        user.authentication_time = timezone.now()
        user.save()

        picture_url = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user.picture.url}" if picture else None
        im_data = {k: v for k, v in {'picture': picture_url, 'nickname': nickname} if v is not None}
        if im_data and not update_im_account_info(user.uid, **im_data):
            transaction.savepoint_rollback(s1)
            return False

        # 提交事务 (如果没有异常,就提交事务)
        transaction.savepoint_commit(s1)
        return True


class GetLawyerFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = LawyerFile
        fields = ['type', 'file', 'is_authentication']


class UserSimpleInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('picture', 'nickname', 'profile', 'gender', 'is_authentication')


class GetLawyerSerializer(serializers.ModelSerializer):
    conversation_types = serializers.SerializerMethodField()
    user = UserSimpleInfoSerializer(read_only=True)
    # picture = serializers.ImageField(source='user.picture')
    # nickname = serializers.CharField(source='user.nickname')
    # profile = serializers.CharField(source='user.profile')
    # is_authentication = serializers.IntegerField(source='user.is_authentication')
    city_ad_code = serializers.IntegerField(source='city.ad_code')
    files = serializers.SerializerMethodField()
    city = serializers.SerializerMethodField()

    def get_city(self, obj):
        return f'{obj.province_name} {obj.city_name}'

    def get_conversation_types(self, obj):
        conversations = ConversationType.objects.filter(is_deleted=False,
                                                        lawyer_conversation_type__lawyer=obj.user).values('id', 'name')
        return list(conversations)

    def get_files(self, obj):
        return GetLawyerFileSerializer(LawyerFile.objects.filter(is_deleted=False, lawyer=obj.user),
                                       many=True, context=self.context).data

    class Meta:
        model = Lawyer
        fields = ('type', 'ID_number', 'certificate_number', 'license_period', 'law_firm', 'city', 'work_address',
                  'wechat_id', 'alipay_id', 'user', 'files', 'conversation_types', 'city_ad_code')


class GetLawyerBasicInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lawyer
        fields = ('certificate_number', 'license_period', 'law_firm', 'work_address', 'type')


class GetLawyerForOrdinaryUserSerializer(serializers.ModelSerializer):
    conversation_types = serializers.SerializerMethodField()
    city = serializers.SerializerMethodField()
    lawyer_user = GetLawyerBasicInfoSerializer(read_only=True)
    # certificate_number = serializers.CharField(source='lawyer_user.certificate_number')
    # license_period = serializers.DateField(source='lawyer_user.license_period')
    # law_firm = serializers.CharField(source='lawyer_user.law_firm')
    # work_address = serializers.CharField(source='lawyer_user.work_address')
    # type = serializers.IntegerField(source='lawyer_user.type')
    is_collected = serializers.SerializerMethodField()

    def get_is_collected(self, obj):
        user = self.context.get('request').user if not isinstance(self.context.get('request').user,
                                                                  AnonymousUser) else None
        if user is None:
            return None
        return UserCollectionLawyer.objects.filter(is_deleted=False, user=user, lawyer=obj).exists()

    def get_city(self, obj):
        return '上海'

    def get_conversation_types(self, obj):
        conversations = ConversationType.objects.filter(is_deleted=False,
                                                        lawyer_conversation_type__lawyer=obj).values('name')
        return [i['name'] for i in conversations]

    class Meta:
        model = Users
        fields = ('city', 'is_online', 'lawyer_user',
                  'picture', 'profile', 'nickname', 'conversation_types', 'gender', 'uid', 'is_collected',
                  'is_authentication', 'id')


class LawyerListInfoSerializer(serializers.ModelSerializer):
    city = serializers.SerializerMethodField()

    def get_city(self, obj):
        return f'{obj.province_name} {obj.city_name}'

    class Meta:
        model = Lawyer
        fields = ('license_period', 'type', 'law_firm', 'city')


class LawyerListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    nickname = serializers.CharField()
    lawyer_user = LawyerListInfoSerializer()
    # license_period = serializers.DateField(source='lawyer_user.license_period')
    # type = serializers.IntegerField(source='lawyer_user.type')
    # law_firm = serializers.CharField(source='lawyer_user.law_firm')
    is_online = serializers.BooleanField()
    picture = serializers.ImageField()

    # conversation_type = serializers.CharField(source='lawyer_user.conversation_type')

    class Meta:
        model = Users
        fields = ('id', 'nickname', 'is_online', 'picture', 'uid', 'lawyer_user')


class UpdateLawyerIsOnlineSerializer(serializers.ModelSerializer):
    is_online = serializers.BooleanField(help_text='是否上线，接受布尔值或者数字0,1')

    class Meta:
        model = Users
        fields = 'is_online',


class EnterpriseFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnterPriseFile
        fields = ('type', 'file')


class EnterpriseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='user.pk')
    is_authentication = serializers.IntegerField(source='user.is_authentication')
    city = serializers.SerializerMethodField()
    files = serializers.SerializerMethodField()

    def get_city(self, obj):
        return f'{obj.province_name} {obj.city_name}'

    def get_files(self, obj):
        return EnterpriseFileSerializer(EnterPriseFile.objects.filter(is_deleted=False, user=obj.user),
                                        many=True, context={'request': self.context['request']}).data

    class Meta:
        model = EnterpriseUser
        fields = ('id', 'name', 'type', 'uniform_social_credit_code', 'city', 'address', 'legal_representative',
                  'files', 'is_authentication')


class EnterpriseCreateSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=120, help_text='公司名称')
    type = serializers.ChoiceField(choices=[1, 2, 3, 4, 5],
                                   help_text='公司类型, 1:无限责任公司、2:有限责任公司、3:两合公司、4:股份有限公司、5:股份两合公司')
    uniform_social_credit_code = serializers.CharField(max_length=120, help_text='统一社会信用代码')
    city = serializers.IntegerField(default=310100, help_text='城市')
    address = serializers.CharField(max_length=150, help_text='地址')
    legal_representative = serializers.CharField(max_length=100, help_text='法定代表人')
    type_1 = serializers.ListField(child=serializers.ImageField(), help_text='营业执照')

    def create(self, validated_data):
        data = validated_data
        user = data['user']
        type_1 = [{'type': 1, 'file': i, 'user': user} for i in data.pop('type_1')]
        # request = data.pop('request')
        nickname = data['name']

        files_model = [EnterPriseFile(**i) for i in type_1]
        # data['conversation_type'] = json.dumps([i.pk for i in conversations], ensure_ascii=False)

        # 设置事务保存点
        s1 = transaction.savepoint()
        Users.objects.filter(pk=user.pk, is_deleted=False).update(nickname=nickname, is_authentication=1)
        user_info = Users.objects.filter(pk=user.pk).first()
        EnterPriseFile.objects.bulk_create(files_model)
        EnterpriseUser.objects.create(**data)

        # picture_url = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user_info.picture.url}"
        nickname = user_info.nickname
        if not update_im_account_info(user_info.uid, nickname=nickname):
            transaction.savepoint_rollback(s1)
            return False
        # 提交事务 (如果没有异常,就提交事务)
        transaction.savepoint_commit(s1)
        return True

    def update(self, instance, validated_data):
        data = validated_data
        user = data['user']
        type_1 = [{'type': 1, 'file': i, 'user': user} for i in data.pop('type_1', [])]
        nickname = data.pop('name', None)

        files_model = [EnterPriseFile(**i) for i in type_1]
        # data['conversation_type'] = json.dumps([i.pk for i in conversations], ensure_ascii=False)

        # 设置事务保存点
        s1 = transaction.savepoint()
        if files_model:
            pass

        Users.objects.filter(pk=user.pk, is_deleted=False).update(nickname=nickname, is_authentication=1)
        user_info = Users.objects.filter(pk=user.pk).first()
        EnterPriseFile.objects.bulk_create(files_model)
        EnterpriseUser.objects.create(**data)

        # picture_url = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user_info.picture.url}"
        nickname = user_info.nickname
        if not update_im_account_info(user_info.uid, nickname=nickname):
            transaction.savepoint_rollback(s1)
            return False
        # 提交事务 (如果没有异常,就提交事务)
        transaction.savepoint_commit(s1)
        return True


class LawyerCaseSerializer(serializers.ModelSerializer):
    # user = serializers.PrimaryKeyRelatedField(queryset=Users.objects.filter(is_deleted=False),help_text='律师')
    # user = serializers.IntegerField()
    case = serializers.CharField(max_length=200, help_text='案例名称')
    content = serializers.CharField(help_text='案例内容')

    class Meta:
        model = LawyerCase
        fields = 'id', 'case', 'content', 'create_time'


class UserCaseCollectionSerializer(serializers.ModelSerializer):
    lawyer = serializers.PrimaryKeyRelatedField(queryset=Users.objects.filter(is_deleted=False, role=300),
                                                help_text='律师id', error_messages={'error': '律师不存在'})
    user = serializers.IntegerField(read_only=True)

    # user = serializers.PrimaryKeyRelatedField(queryset=Users.objects.filter(is_deleted=False, role__in=[100,200]),
    #                                           help_text='用户id')

    class Meta:
        model = UserCollectionLawyer
        fields = 'id', 'lawyer', 'user'


class VerificationCodeSerializer(serializers.Serializer):
    phone = serializers.CharField(help_text='手机号')
    code_type = serializers.ChoiceField(default=1, choices=[1, 2, 3, 4, 5],
                                        help_text='1:验证码或注册登录，2:未登录重置密码, 3:更换手机号旧手机号获取验证码, 4:更换手机号新手机号获取验证码, 5:更换身份')


class CheckOldPhoneSerializer(serializers.Serializer):
    phone = serializers.CharField(help_text='手机号')
    verification_code = serializers.CharField(help_text='验证码')


class ChangePasswordWithNoLoginSerialzier(serializers.Serializer):
    phone = serializers.CharField(help_text='手机号')
    verification_code = serializers.CharField(help_text='验证码')
    password = serializers.CharField(help_text='新密码')

    def update(self, instance, validated_data):
        password = validated_data['password']
        verification_code = validated_data['verification_code']
        cache_key = f'phone_{validated_data["phone"]}_{2}'
        print(cache_key)
        print(verification_code)
        print(cache.get(cache_key))
        if verification_code != cache.get(cache_key):
            raise ValidationError('验证码错误')
        if check_password(password, instance.password):
            raise ValidationError('新密码不能与旧密码相同')
        cache.set(cache_key, '', 0)
        new_password = make_password(password)
        instance.password = new_password
        instance.save()
        Users.objects.filter(unionid=instance.unionid, is_deleted=False).update(password=new_password)
        return instance


class LawyerTempSerializer(serializers.Serializer):
    type = serializers.ChoiceField(default=0, choices=[0, 1], help_text='0表示个人，1表示律所')
    gender = serializers.IntegerField(help_text='性别')
    ID_number = serializers.CharField(help_text='身份证')
    certificate_number = serializers.CharField(help_text='执照证号')
    license_period = serializers.DateField(help_text='执业年限')
    city = serializers.IntegerField(help_text='城市', default=310100)
    work_address = serializers.CharField(help_text='办公地址')
    conversation_types = serializers.ListField(child=serializers.CharField(), help_text='领域类型，类型')
    wechat_id = serializers.CharField(help_text='微信号')
    alipay_id = serializers.CharField(help_text='支付宝账号')
    picture = serializers.ImageField(help_text='律师头像')
    profile = serializers.CharField(help_text='详细信息')
    nickname = serializers.CharField(help_text='名字')
    law_firm = serializers.CharField(help_text='律所', required=False)
    type_1 = serializers.ListField(child=serializers.ImageField(), help_text='职业证图片')
    type_2 = serializers.ListField(child=serializers.ImageField(), help_text='年审页图片')
    type_3 = serializers.ListField(child=serializers.ImageField(), help_text='身份证图片')
    type_4 = serializers.ListField(child=serializers.ImageField(), help_text='营业执照图片', required=False)

    # files = LawyerFileSerializer(many=True, help_text='该字段为数组,内层为对象,字段1，type（1：职业证，2：年审页，3：身份证， 4：营业执照），字段2, file(文件)')
    # types
    # def validate(self, attrs):
    #     if not attrs.get('files'):
    #         raise ValidationError('缺少附件参数')
    #     return attrs

    def create(self, validated_data):
        data = validated_data
        user = data['user']
        conversations_list = json.loads(data.pop('conversation_types')[0])
        type_1 = [{'type': 1, 'file': i, 'lawyer': user} for i in data.pop('type_1')]
        type_2 = [{'type': 2, 'file': i, 'lawyer': user} for i in data.pop('type_2')]
        type_3 = [{'type': 3, 'file': i, 'lawyer': user} for i in data.pop('type_3')]
        type_4 = data.pop('type_4', [])
        type_4 = [{'type': 4, 'file': i, 'lawyer': user} for i in type_4]
        request = data.pop('request')
        city = data.pop('city')
        city = City.objects.filter(ad_code=city).first()
        if city is None or city.level != 'city':
            raise ValidationError('城市错误')
        city_name = city.name
        province = city.parent
        province_name = province.name
        data['city'] = city
        data['city_name'] = city_name
        data['province'] = province
        data['province_name'] = province_name
        # picture = data.pop('picture')
        # nickname = data.pop('nickname')
        # gender = data.pop('gender')
        # profile = data.pop('profile')
        print(type(conversations_list), conversations_list)
        conversations = ConversationType.objects.filter(is_deleted=0, pk__in=conversations_list)
        if len(conversations) != len(conversations_list):
            raise ValidationError('领域类型错误')

        conversations_model = [LawyerConversationTypeTemp(**{"lawyer": user, "conversation_type": i})
                               for i in conversations]
        files_model = [LawyerFileTemp(**i) for i in type_1 + type_2 + type_3 + type_4]
        # data['conversation_type'] = json.dumps([i.pk for i in conversations], ensure_ascii=False)

        # 设置事务保存点
        s1 = transaction.savepoint()
        # Users.objects.filter(pk=user.pk, is_deleted=False).update(picture=picture, nickname=nickname, gender=gender,
        #                                                           is_authentication=1, profile=profile)
        # user.picture = picture
        # user.nickname = nickname
        # user.gender = gender
        user.is_authentication = 1
        user.authentication_time = timezone.now()
        # user.profile = profile
        user.save()
        LawyerConversationTypeTemp.objects.bulk_create(conversations_model)
        LawyerFileTemp.objects.bulk_create(files_model)
        instance = LawyerAuthenticationInfoTemp.objects.create(**data)

        picture_url = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{instance.picture.url}"
        nickname = user.nickname
        if not update_im_account_info(user.uid, nickname=nickname, picture=picture_url):
            transaction.savepoint_rollback(s1)
            return False
        # 提交事务 (如果没有异常,就提交事务)
        transaction.savepoint_commit(s1)
        return True


class UpdateLawyerTempSerializer(serializers.Serializer):
    type = serializers.ChoiceField(default=0, choices=[0, 1], help_text='0表示个人，1表示律所')
    gender = serializers.IntegerField(help_text='性别')
    ID_number = serializers.CharField(help_text='身份证')
    certificate_number = serializers.CharField(help_text='执照证号')
    license_period = serializers.DateField(help_text='执业年限')
    city = serializers.IntegerField(help_text='城市')
    work_address = serializers.CharField(help_text='办公地址')
    conversation_types = serializers.ListField(child=serializers.CharField(), help_text='领域类型，类型')
    wechat_id = serializers.CharField(help_text='微信号')
    alipay_id = serializers.CharField(help_text='支付宝账号')
    picture = serializers.ImageField(help_text='律师头像', allow_null=True)
    profile = serializers.CharField(help_text='详细信息')
    nickname = serializers.CharField(help_text='名字')
    law_firm = serializers.CharField(help_text='律所', required=False)
    type_1 = serializers.ListField(child=serializers.ImageField(), help_text='职业证图片')
    type_2 = serializers.ListField(child=serializers.ImageField(), help_text='年审页图片')
    type_3 = serializers.ListField(child=serializers.ImageField(), help_text='身份证图片')
    type_4 = serializers.ListField(child=serializers.ImageField(), help_text='营业执照图片', required=False)

    def update(self, instance, validated_data):
        data = validated_data
        user = data['user']
        try:
            conversations_list = json.loads(data.pop('conversation_types', [[]])[0])
        except TypeError:
            conversations_list = []
        request = data.pop('request', None)

        type_1 = [{'type': 1, 'file': i, 'lawyer': user} for i in data.pop('type_1', [])]
        type_2 = [{'type': 2, 'file': i, 'lawyer': user} for i in data.pop('type_2', [])]
        type_3 = [{'type': 3, 'file': i, 'lawyer': user} for i in data.pop('type_3', [])]
        type_4 = [{'type': 4, 'file': i, 'lawyer': user} for i in data.pop('type_4', [])]

        city = data.pop('city', None)
        if city is not None:
            city = City.objects.filter(ad_code=city).first()
            if city is None or city.level != 'city':
                raise ValidationError('城市错误')
            city_name = city.name
            province = city.parent
            province_name = province.name
            data['city'] = city
            data['city_name'] = city_name
            data['province'] = province
            data['province_name'] = province_name

        files_model = [LawyerFileTemp(**i) for i in type_1 + type_2 + type_3 + type_4]

        s1 = transaction.savepoint()  # 设置事务保存点

        print(type(conversations_list), conversations_list)
        if conversations_list:
            conversations = ConversationType.objects.filter(is_deleted=0, pk__in=conversations_list)
            if len(conversations) != len(conversations_list):
                raise ValidationError('领域类型错误')
            conversations_model = [LawyerConversationType(**{"lawyer": user, "conversation_type": i}) for i in
                                   conversations]
            # 先删除原先的关系，再重新创新新的关系
            LawyerConversationTypeTemp.objects.filter(lawyer=user, is_deleted=False).update(is_deleted=True)
            LawyerConversationTypeTemp.objects.bulk_create(conversations_model)

        # 先删除律师文件，再重新创建
        LawyerFileTemp.objects.filter(lawyer=user, is_deleted=False).update(is_deleted=True)
        LawyerFileTemp.objects.bulk_create(files_model)
        # files_model = [LawyerFileTemp(**i) for i in type_1 + type_2 + type_3 + type_4]

        # if files_model:
        # LawyerFile.objects.filter(is_deleted=False, lawyer=user).update()
        # LawyerFileTemp.objects.bulk_create(files_model)

        user.is_authentication = user.is_authentication if user.is_authentication in [1, 3] else 4
        user.authentication_time = timezone.now()
        user.save()
        # if any([picture, nickname, gender, profile]):
        # user.picture = picture
        # user.nickname = nickname
        # user.gender = gender
        # user.is_authentication = user.is_authentication if user.is_authentication == 1 else 4
        # user.profile = profile
        # user.save()

        if data:
            for attr, value in data.items():
                setattr(instance, attr, value)

            instance.save()

        # picture = instance.picture
        # picture_url = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user.picture.url}" if picture else None
        # im_data = {k: v for k, v in {'picture': picture_url, 'nickname': instance.nickname}.items() if v is not None}
        # if im_data and not update_im_account_info(user.uid, **im_data):
        #     transaction.savepoint_rollback(s1)
        #     return False

        # 提交事务 (如果没有异常,就提交事务)
        transaction.savepoint_commit(s1)
        return True


class GetLawyerFileTempSerializer(serializers.ModelSerializer):
    class Meta:
        model = LawyerFile
        fields = ['type', 'file', 'is_authentication']


class GetLawyerTempSerializer(serializers.ModelSerializer):
    conversation_types = serializers.SerializerMethodField()
    # user = UserSimpleInfoSerializer(read_only=True)
    # picture = serializers.ImageField(source='user.picture')
    # nickname = serializers.CharField(source='user.nickname')
    # profile = serializers.CharField(source='user.profile')
    # is_authentication = serializers.IntegerField(source='user.is_authentication')
    files = serializers.SerializerMethodField()
    city_ad_code = serializers.IntegerField(source='city.ad_code')
    city = serializers.SerializerMethodField()
    is_authentication = serializers.SerializerMethodField()

    def get_is_authentication(self, obj):
        return obj.user.is_authentication if obj.user.is_authentication not in [4, 5] else 2

    def get_conversation_types(self, obj):
        conversations = ConversationType.objects.filter(is_deleted=False,
                                                        lawyer_conversation_type_temp__is_deleted=False,
                                                        lawyer_conversation_type_temp__lawyer=obj.user).values('id',
                                                                                                               'name')
        return list(conversations)

    def get_files(self, obj):
        return GetLawyerFileTempSerializer(LawyerFileTemp.objects.filter(is_deleted=False, lawyer=obj.user),
                                           many=True, context=self.context).data

    def get_city(self, obj):
        return f'{obj.province_name} {obj.city_name}'

    class Meta:
        model = LawyerAuthenticationInfoTemp
        fields = ('type', 'ID_number', 'certificate_number', 'license_period', 'law_firm', 'city', 'work_address',
                  'wechat_id', 'alipay_id', 'picture', 'gender', 'nickname', 'profile', 'files', 'conversation_types',
                  'city_ad_code', 'is_authentication')


class EnterpriseTempSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=120, help_text='公司名称')
    type = serializers.ChoiceField(choices=[1, 2, 3, 4, 5],
                                   help_text='公司类型, 1:无限责任公司、2:有限责任公司、3:两合公司、4:股份有限公司、5:股份两合公司')
    uniform_social_credit_code = serializers.CharField(max_length=120, help_text='统一社会信用代码')
    city = serializers.IntegerField(default=310100, help_text='城市')
    address = serializers.CharField(max_length=150, help_text='地址')
    legal_representative = serializers.CharField(max_length=100, help_text='法定代表人')
    type_1 = serializers.ListField(child=serializers.ImageField(), help_text='营业执照')

    def create(self, validated_data):
        data = validated_data
        user = data['user']
        type_1 = [{'type': 1, 'file': i, 'user': user} for i in data.pop('type_1')]

        city = data.pop('city')
        city = City.objects.filter(ad_code=city).first()
        if city is None or city.level != 'city':
            raise ValidationError('城市错误')
        city_name = city.name
        province = city.parent
        province_name = province.name
        data['city'] = city
        data['city_name'] = city_name
        data['province'] = province
        data['province_name'] = province_name

        files_model = [EnterPriseFileTemp(**i) for i in type_1]

        # 设置事务保存点
        s1 = transaction.savepoint()
        # Users.objects.filter(pk=user.pk, is_deleted=False).update(is_authentication=1)
        user.is_authentication = 1
        user.authentication_time = timezone.now()
        user.save()
        # user_info = Users.objects.filter(pk=user.pk).first()
        EnterPriseFileTemp.objects.bulk_create(files_model)
        EnterpriseUserTemp.objects.create(**data)

        # picture_url = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user_info.picture.url}"
        # nickname = user_info.nickname
        # if not update_im_account_info(user_info.uid, nickname=nickname):
        #     transaction.savepoint_rollback(s1)
        #     return False
        # 提交事务 (如果没有异常,就提交事务)
        transaction.savepoint_commit(s1)
        return True


class UpdateEnterpriseTempSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=120, help_text='公司名称')
    type = serializers.ChoiceField(choices=[1, 2, 3, 4, 5],
                                   help_text='公司类型, 1:无限责任公司、2:有限责任公司、3:两合公司、4:股份有限公司、5:股份两合公司')
    uniform_social_credit_code = serializers.CharField(max_length=120, help_text='统一社会信用代码')
    city = serializers.IntegerField(default=310100, help_text='城市')
    address = serializers.CharField(max_length=150, help_text='地址')
    legal_representative = serializers.CharField(max_length=100, help_text='法定代表人')
    type_1 = serializers.ListField(child=serializers.ImageField(), help_text='营业执照')

    def update(self, instance, validated_data):
        data = validated_data
        user = data['user']

        city = data.pop('city', None)
        if city is not None:
            city = City.objects.filter(ad_code=city).first()
            if city is None or city.level != 'city':
                raise ValidationError('城市错误')
            city_name = city.name
            province = city.parent
            province_name = province.name
            data['city'] = city
            data['city_name'] = city_name
            data['province'] = province
            data['province_name'] = province_name

        type_1 = [{'type': 1, 'file': i, 'user': user} for i in data.pop('type_1')]
        files_model = [EnterPriseFileTemp(**i) for i in type_1]

        # 设置事务保存点
        s1 = transaction.savepoint()

        user.is_authentication = user.is_authentication if user.is_authentication in [1, 3] else 4
        user.authentication_time = timezone.now()
        user.save()

        EnterPriseFileTemp.objects.filter(user=user, is_deleted=False).update(is_deleted=True)
        EnterPriseFileTemp.objects.bulk_create(files_model)

        if data:
            for attr, value in data.items():
                setattr(instance, attr, value)

            instance.save()

        # picture_url = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user_info.picture.url}"
        # nickname = user_info.nickname
        # if not update_im_account_info(user_info.uid, nickname=nickname):
        #     transaction.savepoint_rollback(s1)
        #     return False
        # 提交事务 (如果没有异常,就提交事务)
        transaction.savepoint_commit(s1)
        return True


class LawyerFileTempSerializer(serializers.ModelSerializer):
    type = serializers.ChoiceField(choices=[1, 2, 3, 4], help_text='1:职业证图片,2:年审页图片,3:身份证图片,4:营业执照图片')
    file = serializers.ImageField(help_text='图片')

    class Meta:
        model = LawyerFileTemp
        fields = ('type', 'file', 'id')


class EnterpriseFileTempSerializer(serializers.ModelSerializer):
    type = serializers.ChoiceField(choices=[1], help_text='1:营业执照', default=1)
    file = serializers.ImageField(help_text='图片')

    class Meta:
        model = EnterPriseFile
        fields = ('type', 'file', 'id')


class GetEnterpriseTempSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='user.pk')
    city = serializers.SerializerMethodField()
    files = serializers.SerializerMethodField()
    city_ad_code = serializers.IntegerField(source='city.ad_code')
    is_authentication = serializers.SerializerMethodField()

    def get_is_authentication(self, obj):
        return obj.user.is_authentication if obj.user.is_authentication not in [4, 5] else 2

    def get_city(self, obj):
        return f'{obj.province_name} {obj.city_name}'

    def get_files(self, obj):
        return EnterpriseFileTempSerializer(EnterPriseFileTemp.objects.filter(is_deleted=False, user=obj.user),
                                            many=True, context={'request': self.context['request']}).data

    class Meta:
        model = EnterpriseUserTemp
        fields = ('id', 'name', 'type', 'uniform_social_credit_code', 'city', 'address', 'legal_representative',
                  'files', 'city_ad_code', 'is_authentication')


class GetEnterpriseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='user.pk')
    city = serializers.SerializerMethodField()
    files = serializers.SerializerMethodField()
    city_ad_code = serializers.IntegerField(source='city.ad_code')
    is_authentication = serializers.SerializerMethodField()

    def get_is_authentication(self, obj):
        return obj.user.is_authentication if obj.user.is_authentication not in [4, 5] else 2

    def get_city(self, obj):
        return f'{obj.province_name} {obj.city_name}'

    def get_files(self, obj):
        return EnterpriseFileSerializer(EnterPriseFile.objects.filter(is_deleted=False, user=obj.user),
                                        many=True, context={'request': self.context['request']}).data

    class Meta:
        model = EnterpriseUser
        fields = ('id', 'name', 'type', 'uniform_social_credit_code', 'city', 'address', 'legal_representative',
                  'files', 'city_ad_code', 'is_authentication')


class WeixinLoginSerializer(serializers.Serializer):
    code = serializers.CharField(help_text='微信授权的code')


class AlipayLoginSerializer(serializers.Serializer):
    code = serializers.CharField(help_text='支付宝的code')


class AlipayAuthSignSerializer(serializers.Serializer):
    apiname = serializers.HiddenField(default='com.alipay.account.auth')
    method = serializers.HiddenField(default='alipay.open.auth.sdk.code.get')
    app_id = serializers.HiddenField(default=app_id)
    app_name = serializers.HiddenField(default='mc')
    biz_type = serializers.HiddenField(default='openservice')
    pid = serializers.HiddenField(default='2088141319282603')
    product_id = serializers.HiddenField(default='APP_FAST_LOGIN')
    scope = serializers.HiddenField(default='kuaijie')
    target_id = serializers.CharField()
    auth_type = serializers.HiddenField(default='AUTHACCOUNT')
    sign_type = serializers.HiddenField(default='RSA2')
    sign = serializers.CharField(required=False)


class TestSerializer(serializers.Serializer):
    p1 = serializers.CharField()

    # p1 = serializers.ListField(child=serializers.FileField())
    # p2 = serializers.ListField(child=serializers.FileField())

    def create(self, validated_data):
        raise ValidationError('dadfaf')
        # user = self.context.get('request').user
        #
        # payload = jwt_payload_handler(user)
        #
        # return {
        #     'token': jwt_encode_handler(payload),
        #     'user': user
        # }


def copy_data():
    lawyers = Lawyer.objects.filter(is_deleted=False, user__id__gt=0)
    lawyer_files = LawyerFile.objects.filter(is_deleted=False)
    lawyer_conversations = LawyerConversationType.objects.filter(is_deleted=False)
    enterprises = EnterpriseUser.objects.filter(is_deleted=False, user__id__gt=0)
    enterprise_files = EnterPriseFile.objects.filter(is_deleted=False)

    for i in lawyers:
        user = i.user
        lawyer_temp = LawyerAuthenticationInfoTemp()
        lawyer_temp.type = i.type
        lawyer_temp.ID_number = i.ID_number
        lawyer_temp.certificate_number = i.certificate_number
        lawyer_temp.license_period = i.license_period
        lawyer_temp.city = i.city
        lawyer_temp.city_name = i.city.name
        lawyer_temp.province = i.city.parent
        lawyer_temp.province_name = i.city.parent.name
        lawyer_temp.work_address = i.work_address
        lawyer_temp.wechat_id = i.wechat_id
        lawyer_temp.alipay_id = i.alipay_id
        lawyer_temp.law_firm = i.law_firm
        lawyer_temp.user = i.user
        lawyer_temp.picture = user.picture
        lawyer_temp.profile = user.profile
        lawyer_temp.nickname = user.nickname
        lawyer_temp.gender = user.gender
        lawyer_temp.save()

    for i in lawyer_files:
        temp = LawyerFileTemp()
        temp.lawyer = i.lawyer
        temp.file = i.file
        temp.type = i.type
        temp.is_authentication = 1
        temp.save()

    for i in lawyer_conversations:
        temp = LawyerConversationTypeTemp()
        temp.lawyer = i.lawyer
        temp.conversation_type = i.conversation_type
        temp.save()

    for i in enterprises:
        temp = EnterpriseUserTemp()
        temp.user = i.user
        temp.name = i.name
        temp.type = i.type
        temp.uniform_social_credit_code = i.uniform_social_credit_code
        temp.city = i.city
        temp.city_name = i.city_name
        temp.province = i.province
        temp.province_name = i.province_name
        temp.address = i.address
        temp.legal_representative = i.legal_representative
        temp.save()

    for i in enterprise_files:
        temp = EnterPriseFileTemp()
        temp.user = i.user
        temp.file = i.file
        temp.type = i.type
        temp.is_authentication = 1
        temp.save()
