# from laws_online.libs.utils.tools import Response
from django.contrib.auth import get_user_model
from rest_framework_sso import claims
from rest_framework import exceptions
from django.utils.translation import gettext as _
from laws_online.settings.base import MEDIA_URL
from laws_online.libs.utils.tencent_im import TLSSigAPIv2
import random
import requests
import json
import time
import os

def jwt_response_payload_handler(token, user=None, request=None):
    """
    自定义jwt认证成功返回数据
    :token  返回的jwt
    :user   当前登录的用户信息[对象]
    :request 当前本次客户端提交过来的数据
    """
    return {
        'code': 0,
        'msg': 'SUCCESS',
        'data': {
            'token': token,
            'id': user.id,
            'uid': user.uid,
            'nickname': user.nickname,
            'role': user.role,
            'phone': user.phone,
            'picture': f"{request.META['wsgi.url_scheme']}://{request.get_host()}{user.picture.url}" if user.picture else None,
            'user_sig': get_user_sig(user.uid),
            'is_online': user.is_online,
            'is_authentication': user.is_authentication if user.is_authentication not in [4, 5] else 2,
            'is_new': user.is_new
        }
    }


CODE_TYPE = (
    (1, '登录或注册验证'),
    (2, '未登录更换密码'),
    (3, '更换手机，验证旧手机'),
    (4, '更换手机，验证新手机'),
    (5, '更换手机，验证旧手机后的凭证'),
)

# def jwt_response_payload_error_handler(serializer, request=None):
#
#     return {
#         "msg": "用户名或者密码错误",
#         "status": 400,
#         "detail": serializer.errors
#     }

env = os.environ.get('DJANGO_SETTINGS_MODULE')

if env in ['laws_online.settings.master']:
    sdkappid = '1400520804'
    identifier = 'administrator'
    key = '4f19cceaece6013d420edc3902e25a4c17a6910bbe35cee3aec12139503a28fa'
else:
    # sdkappid = '1400520804'
    # identifier = 'administrator'
    # key = '4f19cceaece6013d420edc3902e25a4c17a6910bbe35cee3aec12139503a28fa'
    sdkappid = '1400470527'
    identifier = 'administrator'
    key = '088f438f4c7b8598c6ac5401fa6aca950ea1c794720b4b6fb01d28b2ab206c38'


DEFAULT_MANAGER_GROUP = {
    'LawyerAuthentication': '律师审核',
    'EnterpriseAuthentication': '企业用户审核',
    'LawyerComplaint': '律师投诉',
    'UserComplaint': '用户投诉',
    'PlatformComplaintSuggestion': '平台投诉'
}


def get_user_sig(user_identifier):
    api = TLSSigAPIv2(sdkappid, key)
    return api.genUserSig(user_identifier)


def add_im_account(Identifier, Nick, FaceUrl=None):
    api = TLSSigAPIv2(sdkappid, key)
    usersig = api.genUserSig(identifier)
    random_int = str(random.randint(0, 4294967295))
    url = 'https://console.tim.qq.com/v4/im_open_login_svc/account_import?sdkappid='
    url += f'{sdkappid}&identifier={identifier}&usersig={usersig}&random={random_int}&contenttype=json'
    data = {'Identifier': Identifier, 'Nick': Nick}
    data.update({'FaceUrl': FaceUrl}) if FaceUrl else 0
    # j = json.dumps(data, ensure_ascii=False)
    response = requests.post(url, json=data)
    print(url)
    print(data)
    response_data = json.loads(response.text)
    print(response_data)
    if response_data.get('ErrorCode') == 0:
        return True
    return False


def update_im_account_info(Identifier, **kwargs):
    api = TLSSigAPIv2(sdkappid, key)
    usersig = api.genUserSig(identifier)
    random_int = str(random.randint(0, 4294967295))
    url = 'https://console.tim.qq.com/v4/profile/portrait_set?sdkappid='
    url += f'{sdkappid}&identifier={identifier}&usersig={usersig}&random={random_int}&contenttype=json'
    data = {
        "From_Account": Identifier,
        "ProfileItem": []
    }
    if kwargs.get('nickname'):
        data['ProfileItem'].append({
            "Tag": "Tag_Profile_IM_Nick",
            "Value": kwargs.get('nickname')
        })
    if kwargs.get('picture'):
        data['ProfileItem'].append({
            "Tag": "Tag_Profile_IM_Image",
            "Value": kwargs.get('picture')
        })

    response = requests.post(url, json=data)
    response_data = json.loads(response.text)
    if response_data.get('ErrorCode') == 0:
        return True
    return False


def post_im_message(from_user, to_user, message, msg_type='custom'):
    api = TLSSigAPIv2(sdkappid, key)
    usersig = api.genUserSig(identifier)
    random_int = random.randint(0, 4294967295)
    url = 'https://console.tim.qq.com/v4/openim/sendmsg?sdkappid='
    url += f'{sdkappid}&identifier={identifier}&usersig={usersig}&random={random_int}&contenttype=json'
    data = {
        "SyncOtherMachine": 1,
        "From_Account": from_user,
        "To_Account": to_user,
        "MsgRandom": random_int,
        "MsgTimeStamp": int(time.time()),
    }

    if msg_type == 'custom':
        msg_body = [
            {
                "MsgType": "TIMCustomElem",
                "MsgContent": {
                    "Data": message if isinstance(message, str) else json.dumps(message, ensure_ascii=False),
                    "Desc": "notification",
                    "Ext": "url",
                    "Sound": "dingdong.aiff"
                }
            }
        ]
    elif msg_type == 'text':
        msg_body = [
            {
                "MsgType": "TIMTextElem",
                "MsgContent": {
                    "Text": message,
                }
            }
        ]
    else:
        return False

    data['MsgBody'] = msg_body

    response = requests.post(url, json=data)
    response_data = json.loads(response.text)
    if response_data.get('ErrorCode') == 0:
        print(response_data)
        return True
    print(response_data)
    return False


def post_im_message_in_group(group_id, message, msg_type='custom'):
    api = TLSSigAPIv2(sdkappid, key)
    usersig = api.genUserSig(identifier)
    random_int = random.randint(0, 4294967295)
    url = 'https://console.tim.qq.com/v4/group_open_http_svc/send_group_msg?sdkappid='
    url += f'{sdkappid}&identifier={identifier}&usersig={usersig}&random={random_int}&contenttype=json'
    data = {
        "GroupId": group_id,
        "Random": random_int,  # 随机数字，五分钟数字相同认为是重复消息
    }
    if msg_type == 'custom':
        data['MsgBody'] = [  # 消息体，由一个 element 数组组成，详见字段说明
            {
                "MsgType": "TIMCustomElem",
                "MsgContent": {
                    "Data": message if isinstance(message, str) else json.dumps(message, ensure_ascii=False),
                    "Desc": "notification",
                    "Ext": "url",
                    "Sound": "dingdong.aiff"
                }
            }
        ]
    elif msg_type == 'text':
        data['MsgBody'] = [  # 消息体，由一个 element 数组组成，详见字段说明
            {
                "MsgType": "TIMTextElem",  # 文本
                "MsgContent": {
                    "Text": "red packet"
                }
            }
        ]
    else:
        return False

    response = requests.post(url, json=data)
    response_data = json.loads(response.text)
    if response_data.get('ErrorCode') == 0:
        print(response_data)
        return True
    print(response_data)
    return False


def set_im_info(uid, nickname=None, image=None, role=None, level=None):
    params = locals()
    params.pop('uid', None)
    tag_dict = {
        'nickname': 'Tag_Profile_IM_Nick',
        'image': 'Tag_Profile_IM_Image',
        'role': 'Tag_Profile_IM_Role',
        'level': 'Tag_Profile_IM_Level',
    }
    api = TLSSigAPIv2(sdkappid, key)
    usersig = api.genUserSig(identifier)
    random_int = random.randint(0, 4294967295)
    url = 'https://console.tim.qq.com/v4/profile/portrait_set?sdkappid='
    url += f'{sdkappid}&identifier={identifier}&usersig={usersig}&random={random_int}&contenttype=json'
    data = {
        "From_Account": uid,
        "ProfileItem": []
    }
    data['ProfileItem'] = [{"Tag": tag_dict[k], "Value": v} for k, v in params.items() if v is not None]
    print(data)
    response = requests.post(url, json=data)
    response_data = json.loads(response.text)
    if response_data.get('ErrorCode') == 0:
        print(response_data)
        return True
    print(response_data, uid)
    return False


def check_im_online(uids: list) -> list:
    api = TLSSigAPIv2(sdkappid, key)
    usersig = api.genUserSig(identifier)
    random_int = random.randint(0, 4294967295)
    url = 'https://console.tim.qq.com/v4/openim/querystate?sdkappid='
    url += f'{sdkappid}&identifier={identifier}&usersig={usersig}&random={random_int}&contenttype=json'
    data = {
        "To_Account": uids if len(uids) <= 500 else uids[0:500]
    }
    response = requests.post(url, json=data)
    response_data = json.loads(response.text)
    if response_data.get('ErrorCode') == 0:
        result = response_data.get('QueryResult')
        return [i['To_Account'] for i in result if i['Status'] in ['Online', 'PushOnline']]

    return []


def remove_im_account(uids: []):
    api = TLSSigAPIv2(sdkappid, key)
    usersig = api.genUserSig(identifier)
    random_int = random.randint(0, 4294967295)
    url = 'https://console.tim.qq.com/v4/im_open_login_svc/account_delete?sdkappid='
    url += f'{sdkappid}&identifier={identifier}&usersig={usersig}&random={random_int}&contenttype=json'
    data = {
        "DeleteItem":
            [{"UserID": i} for i in uids]
    }
    response = requests.post(url, json=data)
    response_data = json.loads(response.text)
    if response_data.get('ErrorCode') == 0:
        print(response_data)
        return True
    print(response_data)
    return False

def get_user_profile(uid):
    api = TLSSigAPIv2(sdkappid, key)
    usersig = api.genUserSig(identifier)
    random_int = str(random.randint(1, 99999999))
    url = 'https://console.tim.qq.com/v4/profile/portrait_get?sdkappid='
    url += f'{sdkappid}&identifier={identifier}&usersig={usersig}&random={random_int}&contenttype=json'
    data = {
        "To_Account": [uid],
        "TagList":
            [
                "Tag_Profile_IM_Nick",
                "Tag_Profile_IM_AllowType",
                "Tag_Profile_IM_SelfSignature",
                "Tag_Profile_IM_Role"
                # "Tag_Profile_Custom_Test"
            ]
    }
    response = requests.post(url, json=data)
    response_data = json.loads(response.text)
    print(response_data)
    if response_data.get('ErrorCode') == 0:
        return True
    return False


def jwt_get_user_secret(user):
    return user.uid


def create_unionid():
    from laws_online.apps.users.models import Users
    import shortuuid
    phones = Users.objects.filter(is_deleted=False).values('phone')
    phones = list(set(i['phone'] for i in phones))
    for phone in phones:
        u = Users.objects.filter(phone=phone, is_deleted=False).update(unionid=shortuuid.uuid())
        print(u)

if __name__ == '__main__':
    # l = ['bCqwtxJ6khxPwBUzGWfV9y', 'RWqSH3KckxTq6fbFfZpaEE', 'mkRPnXJ6twtt5upKsXfXAj', 'FV4szRxH72txHbVqkQVA9K', 'JbBQ47TU8cdDiTsDCi4maF', 'Mptydi58HXe25uGuBDptNg', '2zYhnHnxNNfRBiWUZcWpyN', 'ZQ5sBkhM6UWYpjrMHVxo7Z', 'QJbR44AK9KfSmp5B5kiucj', 'QLRqFoMuzkiDQukBLo6i67']
    # post_im_message(l[0], l[1], '1111', 'text')
    # from laws_online.apps.users.models import Users
    # users = Users.objects.filter(uid__in=l, is_deleted=False)
    # for user in users:
    #     picture = f"https://api.zfzxsh.com/{user.picture.url}" if user.picture else None
    #     add_im_account(user.uid, user.nickname, picture)
    #     set_im_info(user.uid, role=user.role)

    # l = ['GTvZsQGbMoW5SVF4RcKuUk', 'myNcw7tUkdztob32gmAnB8']
    # for i in l:
    #     set_im_info(i, nickname='在线客服')
        # set_im_info(i, nickname='酨姭䗘𤒓')
    # remove_im_account(['test', 'accept', 'from', 'to', 'Kgi58P5fjxqQWjVPPMHDpf', '8eoMTkasuoLcmgvkEuyCYG', 'X4oDUTQ9cDcz76h5AuVwAx', 'ngV85L4zMRD6yiPqKXMNGK', '58fGrCKZ6a8sfKptTiQxEo', 'KeKMuxNpEhfhcAh8kvRX9k', 'hGQzBzwAmN82iw9wJ2J2K5', 'fFVaUpBdG6UxLpAyzfJcF8', 'a4QD6rjnC75VBUrRaGGXLb', 'kzw7YrYwvEMQ7U7LbXEYsU', 'Kc77VBgUKxGHkEn9YuWMHz', 'd6XZxcwuR9nz8t339J2qdB', 'hZH9VBFx2ht97m63HdLvug', '8JMCQwbv4hpPkasRMtoNSL', '29mDaYrUD5fkQEFN4o44yZ', '8UMCmnNwUGfnDoGUxFTLdC', 'EW72TZXaaUZVFeGX4bMPwg', '9XSyBsPsJd4JgwjAtxJwHu', '9ruuZM8e9wdqtaT6HRPyKm', 'bKA2wdAdv9fE3RfGXbJkcT', 'Yhbk5KmsJSBybn7oFZjW3d', 'KKsoGKRqDw3FVvgeViZJQ7', 'ZxwbsE79cQiYsaRzJptXsw', '4wLrNttxETvF3F3B4x7TmV', '5zRgKAuHRJf3GGHUALoJR5', '24YYpZb4dZxT8vbjyZLDH9', '3tmQbkYJEPc4TSc4MDFQde', 'cwnMv7vw4nP7Lc7YReEtPf', 'MRC5og7awJdHQu8rVHYeVS', 'dpXdkYQVQb8H2twfh5v6M7', 'mfP9gd8TzFLVPFvTVTaQKq', 'NxiboTGWUmFRZfMfPmvDWC', 'ATuUdgwxCuusKJBGZgHjVv', 'cciDEvz4ZpY64KBtD4xiF9', 'U8qvRVWn7GyCEPV95Wyphm', 'CGcKsQiGZDJhqHaEJcjVwz', 'bwnBC4wKVnNt25vZFGgKMj', 'Pqm6xT7MwpJKuehsTHL6Ui', 'RoA9SrKqhnEHAfNQv87QfC', '9LhGxyVsgd2WRXVZRkdeCd', '6upF8k6RLhYoEMRA6eSa8o', 'oDZrNSzFM6H4e8R2DkQGCX', 'bfe57oXA6vq3RAxRmgXMdH', 'ncff4AAifSnahgdZhheSMy', 'NP7577VYM75HFXHzaQcCeS', 'PwvHYsZwFAMph5tdM6q8oN', 'U8bh8zM3q8WzHxcZx4vF6T', 'AbecoqfLURB89Jtfmy4ViV', 'kYzCSP5fhbsLDD4JNo48Vp', 'Bzsa2iuCobwpHp6aT4oJz5'])
    # post_im_message('cwnMv7vw4nP7Lc7YReEtPf', 'GTvZsQGbMoW5SVF4RcKuUk', '六六六', 'text')
    # manager_list = ['manager', 'GTvZsQGbMoW5SVF4RcKuUk', 'myNcw7tUkdztob32gmAnB8']
    # r = [set_im_info(i, role=1000) for i in manager_list]
    # post_im_message('KKsoGKRqDw3FVvgeViZJQ7', 'GTvZsQGbMoW5SVF4RcKuUk', '臭傻逼', 'text')
    # l = [{'uid': 'master', 'role': 1000}, {'uid': 'test', 'role': 1000}, {'uid': 'accept', 'role': 1000}, {'uid': 'from', 'role': 1000}, {'uid': 'to', 'role': 1000}, {'uid': 'Kgi58P5fjxqQWjVPPMHDpf', 'role': 100}, {'uid': '8eoMTkasuoLcmgvkEuyCYG', 'role': 100}, {'uid': 'X4oDUTQ9cDcz76h5AuVwAx', 'role': 300}, {'uid': 'ngV85L4zMRD6yiPqKXMNGK', 'role': 300}, {'uid': '58fGrCKZ6a8sfKptTiQxEo', 'role': 300}, {'uid': 'KeKMuxNpEhfhcAh8kvRX9k', 'role': 300}, {'uid': 'hGQzBzwAmN82iw9wJ2J2K5', 'role': 300}, {'uid': 'fFVaUpBdG6UxLpAyzfJcF8', 'role': 200}, {'uid': 'a4QD6rjnC75VBUrRaGGXLb', 'role': 100}, {'uid': 'mo7FymtUnfSNG5MtYw3sGG', 'role': 100}, {'uid': 'kzw7YrYwvEMQ7U7LbXEYsU', 'role': 100}, {'uid': 'Kc77VBgUKxGHkEn9YuWMHz', 'role': 300}, {'uid': 'd6XZxcwuR9nz8t339J2qdB', 'role': 300}, {'uid': 'hZH9VBFx2ht97m63HdLvug', 'role': 300}, {'uid': 'ByViSxb4DaX37NEdxEX93g', 'role': 300}, {'uid': '8JMCQwbv4hpPkasRMtoNSL', 'role': 300}, {'uid': '29mDaYrUD5fkQEFN4o44yZ', 'role': 300}, {'uid': '8UMCmnNwUGfnDoGUxFTLdC', 'role': 300}, {'uid': 'EW72TZXaaUZVFeGX4bMPwg', 'role': 300}, {'uid': '9XSyBsPsJd4JgwjAtxJwHu', 'role': 100}, {'uid': '9ruuZM8e9wdqtaT6HRPyKm', 'role': 100}, {'uid': 'bKA2wdAdv9fE3RfGXbJkcT', 'role': 300}, {'uid': '6ShkeRC2ZHsFiDHMgUYZyd', 'role': 100}, {'uid': 'Yhbk5KmsJSBybn7oFZjW3d', 'role': 300}, {'uid': 'KKsoGKRqDw3FVvgeViZJQ7', 'role': 100}, {'uid': 'ZxwbsE79cQiYsaRzJptXsw', 'role': 300}, {'uid': '4wLrNttxETvF3F3B4x7TmV', 'role': 200}, {'uid': '5zRgKAuHRJf3GGHUALoJR5', 'role': 200}, {'uid': '24YYpZb4dZxT8vbjyZLDH9', 'role': 200}, {'uid': '3tmQbkYJEPc4TSc4MDFQde', 'role': 100}, {'uid': 'cwnMv7vw4nP7Lc7YReEtPf', 'role': 300}]
    # add_im_account('ngV85L4zMRD6yiPqKXMNGK', '李伟')
    # r = [set_im_info(i['uid'], role=i['role']) for i in l]
    # set_im_info('cwnMv7vw4nP7Lc7YReEtPf', role=300, level=300)
    # m1 = {
    #     "type": "100",
    #     "title": "支付账单",
    #     "conversation": 1,
    #     "conversation_type": "臭傻逼",
    #     "amount": "100.10",
    #     'payment_id': 10,
    #     'out_trade_no': 4450175759923810305,
    #     'content': '臭傻逼',
    # }
    # print(post_im_message('8eoMTkasuoLcmgvkEuyCYG', 'Kgi58P5fjxqQWjVPPMHDpf', m1))

    c1 = {
        'type': 201,
        'title': '咨询订单',
        'conversation': 1,
        'conversation_type': '111',
        'content': '11111'
    }
    # print(post_im_message('8eoMTkasuoLcmgvkEuyCYG', 'X4oDUTQ9cDcz76h5AuVwAx', c1))
    m1 = im_data = {
        'type': 200,
        'title': '咨询订单',
        'conversation': 62,
        'conversation_type': '公司顾问',
        'content': '123'
    }
    # r = post_im_message('a4QD6rjnC75VBUrRaGGXLb', 'GTvZsQGbMoW5SVF4RcKuUk', '臭傻逼', 'text')
    # print(r)
    # post_im_message_in_group('managers', 'qwer', 'text')

# def authenticate_payload(payload):
#     user_model = get_user_model()
#     user, created = user_model.objects.get_or_create(
#         service=payload.get(claims.ISSUER),
#         external_id=payload.get(claims.USER_ID),
#     )
#     if not user.is_active:
#         raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))
#     return user
