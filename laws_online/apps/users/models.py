from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractUser, UserManager as _UserManager
from laws_online.apps.city.models import City
from shortuuidfield import ShortUUIDField
from django.utils.translation import gettext as _
from django.utils import timezone
from laws_online.settings.base import MEDIA_ROOT
import os


# Create your models here.

class UserManager(BaseUserManager):  #自定义Manager管理器
    # use_in_migrations = True
    def _create_user(self, password, **kwargs):
        # if not username:
        #     raise ValueError("请输入用户名！")
        if not password:
            raise ValueError("请输入密码！")
        # if not email:
        #     raise ValueError("请输入邮箱地址！")
        user = self.model(role=1000, gender=1, **kwargs)
        user.set_password(password)
        user.save()
        print(21, user.uid)
        return user

    def create_user(self, password, **kwargs):  # 创建普通用户
        kwargs['is_superuser'] = False
        return self._create_user(password, **kwargs)

    def create_superuser(self, uid, password, **kwargs):  # 创建超级用户
        print(29, kwargs)
        kwargs['is_superuser'] = True
        kwargs['is_staff'] = True
        return self._create_user(password, **kwargs)


class Users(AbstractUser):  # 自定义User
    GENDER_TYPE = (
        (1, "男"),
        (0, "女")
    )
    ROLE_TYPE = (
        (100, '普通用户'),
        (200, '公司用户'),
        (300, '律师'),
        (1000, '管理人员')
    )
    STATUS = (
        (0, '正常'),
        (1, '暂停'),
        (2, '封停')
    )
    username = models.CharField(_('username'), max_length=100)
    nickname = models.CharField(max_length=50, verbose_name="昵称")
    age = models.IntegerField(verbose_name="年龄", default=0)
    gender = models.IntegerField(choices=GENDER_TYPE, verbose_name="性别", default=1)
    phone = models.CharField(max_length=13, verbose_name="手机号码", db_index=True)
    email = models.EmailField(verbose_name="邮箱", default='')
    picture = models.ImageField(upload_to="users/picture", verbose_name="用户头像", default='default/avatar.png')
    home_address = models.CharField(max_length=100, null=True, blank=True, verbose_name="地址")
    uid = ShortUUIDField(verbose_name='用户唯一身份', unique=True, db_index=True)

    unionid = ShortUUIDField(verbose_name='同一手机号的用户唯一身份', db_index=True, null=True)

    # card_id = models.CharField(max_length=30, verbose_name="身份证", null=True, blank=True)
    role = models.IntegerField(verbose_name='身份', choices=ROLE_TYPE, default=100)
    is_active = models.BooleanField(default=True, verbose_name="激活状态")
    is_staff = models.BooleanField(default=False, verbose_name="是否是员工")
    is_online = models.BooleanField(default=False, verbose_name='是否在线')
    is_authentication = models.IntegerField(default=0, verbose_name='''是否认证, 0表示未提交认证， 1表示已提交未审核， 
                                2表示已提交已审核，3表示已提交被驳回, 4表示审核通过后的重新提交未审核, 5表示审核通过后重新提交被驳回''')
    authentication_time = models.DateTimeField(null=True, verbose_name='认证提交时间')
    profile = models.TextField(blank=True, default='')
    status = models.IntegerField(default=0, verbose_name='可用状态, 0表示可用，1表示暂停，2表示不可用')
    suspend_end_time = models.DateField(null=True, verbose_name='过期时间', blank=True)

    is_new = models.BooleanField(default=True)
    # date_joined = models.DateTimeField(auto_now_add=True)
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    update_by = models.IntegerField(verbose_name='更新者id', default=0)
    is_deleted = models.BooleanField(default=0)

    USERNAME_FIELD = 'uid'  # 使用authenticate验证时使用的验证字段，可以换成其他字段，但验证字段必须是唯一的，即设置了unique=True
    REQUIRED_FIELDS = ['phone']  # 创建用户时必须填写的字段，除了该列表里的字段还包括password字段以及USERNAME_FIELD中的字段
    EMAIL_FIELD = 'email'  # 发送邮件时使用的字段

    objects = UserManager()

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')


class Lawyer(models.Model):
    type = models.IntegerField(default=0)
    ID_number = models.CharField(max_length=100)
    certificate_number = models.CharField(max_length=100)
    license_period = models.DateField()
    city = models.ForeignKey(City, null=True, db_constraint=False, on_delete=models.DO_NOTHING,
                             related_name='lawyer_city')
    city_name = models.CharField(max_length=50, default='')
    province = models.ForeignKey(City, null=True, db_constraint=False, on_delete=models.DO_NOTHING,
                                 related_name='lawyer_province')
    province_name = models.CharField(max_length=50, default='')
    work_address = models.CharField(max_length=200)
    # conversation_type = models.ManyToManyField(to='conversation.ConversationType',
    #                                            related_name='conversation_type_user', blank=True)
    wechat_id = models.CharField(max_length=120, default='')
    alipay_id = models.CharField(max_length=120, default='')
    law_firm = models.CharField(max_length=200, default='')
    user = models.OneToOneField(Users, db_constraint=False, on_delete=models.DO_NOTHING, related_name='lawyer_user')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True, null=True)
    # conversation_type = models.TextField(default='')
    update_time = models.DateTimeField(auto_now=True)
    # conversation_type = models.ForeignKey('conversation.ConversationType', db_constraint=False,
    #                                       on_delete=models.DO_NOTHING, related_name='lawyer_conversation_type')


class LawyerConversationType(models.Model):
    lawyer = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING, related_name='lawyer_type_user')
    conversation_type = models.ForeignKey('conversation.ConversationType', db_constraint=False,
                                          on_delete=models.DO_NOTHING, related_name='lawyer_conversation_type')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)


class LawyerFile(models.Model):
    lawyer = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING, related_name='lawyer_file')
    file = models.FileField(upload_to='users/lawyers', null=True, verbose_name='文件')
    type = models.IntegerField()  # 1：职业证，2：年审页，3：身份证， 4：营业执照， ，
    is_authentication = models.IntegerField(default=0, verbose_name='''temp表的对应id''')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)

    def delete(self, using=None, keep_parents=False):
        os.remove(MEDIA_ROOT + self.file.path)
        return super().delete(using=None, keep_parents=False)


class EnterpriseUser(models.Model):
    user = models.OneToOneField(Users, db_constraint=False, on_delete=models.DO_NOTHING, related_name='enterprise_user')
    name = models.CharField(max_length=120, verbose_name='公司名称')
    type = models.IntegerField(verbose_name='公司类型, 1:无限责任公司、2:有限责任公司、3:两合公司、4:股份有限公司、5:股份两合公司')
    uniform_social_credit_code = models.CharField(max_length=120, verbose_name='统一社会信用代码')
    city = models.ForeignKey(City, null=True, db_constraint=False, on_delete=models.DO_NOTHING, verbose_name='城市',
                             related_name='enterprise_city')
    city_name = models.CharField(max_length=50, default='')
    province = models.ForeignKey(City, null=True, db_constraint=False, on_delete=models.DO_NOTHING,
                                 related_name='enterprise_province')
    province_name = models.CharField(max_length=50, default='')
    address = models.CharField(max_length=150, verbose_name='地址')
    legal_representative = models.CharField(max_length=100, verbose_name='法定代表人')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True, null=True)
    update_time = models.DateTimeField(auto_now=True)


class EnterPriseFile(models.Model):
    user = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING, related_name='enterprise_file')
    file = models.FileField(upload_to='users/enterprise', null=True, verbose_name='文件')
    type = models.IntegerField()  # 1：business_license 营业执照
    is_authentication = models.IntegerField(default=0, verbose_name='''temp表的对应id''')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)


class LawyerCase(models.Model):
    user = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING, related_name='lawyer_case')
    case = models.CharField(max_length=200, verbose_name='案例名称')
    content = models.TextField(blank=True, verbose_name='案例内容')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)


class UserCollectionLawyer(models.Model):
    user = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING, related_name='user_collection')
    lawyer = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING,
                               related_name='collection_lawyer')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)


# 临时未审核的信息表
class LawyerAuthenticationInfoTemp(models.Model):
    type = models.IntegerField(default=0)
    ID_number = models.CharField(max_length=100)
    certificate_number = models.CharField(max_length=100)
    license_period = models.DateField()
    city = models.ForeignKey(City, null=True, db_constraint=False, on_delete=models.DO_NOTHING,
                             related_name='lawyer_city_temp')
    city_name = models.CharField(max_length=50, default='')
    province = models.ForeignKey(City, null=True, db_constraint=False, on_delete=models.DO_NOTHING,
                                 related_name='lawyer_province_temp')
    province_name = models.CharField(max_length=50, default='')
    work_address = models.CharField(max_length=200)
    # conversation_type = models.ManyToManyField(to='conversation.ConversationType',
    #                                            related_name='conversation_type_user', blank=True)
    wechat_id = models.CharField(max_length=120, default='')
    alipay_id = models.CharField(max_length=120, default='')
    law_firm = models.CharField(max_length=200, default='')
    user = models.OneToOneField(Users, db_constraint=False, on_delete=models.DO_NOTHING, related_name='lawyer_temp')
    picture = models.ImageField(upload_to="users/picture", verbose_name="用户头像")
    profile = models.TextField(blank=True, default='')
    nickname = models.CharField(max_length=50, verbose_name="昵称")
    gender = models.IntegerField(verbose_name="性别", default=1)
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True, null=True)
    # conversation_type = models.TextField(default='')
    update_time = models.DateTimeField(auto_now=True)


class LawyerFileTemp(models.Model):
    lawyer = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING, related_name='lawyer_file_temp')
    file = models.FileField(upload_to='users/lawyers', null=True, verbose_name='文件')
    type = models.IntegerField()  # 1：职业证，2：年审页，3：身份证， 4：营业执照， ，
    is_authentication = models.IntegerField(default=0, verbose_name='''0表示未审核''')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)


class LawyerConversationTypeTemp(models.Model):
    lawyer = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING,
                               related_name='lawyer_type_user_temp')
    conversation_type = models.ForeignKey('conversation.ConversationType', db_constraint=False,
                                          on_delete=models.DO_NOTHING, related_name='lawyer_conversation_type_temp')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)


class EnterpriseUserTemp(models.Model):
    user = models.OneToOneField(Users, db_constraint=False, on_delete=models.DO_NOTHING,
                            related_name='enterprise_user_temp')
    name = models.CharField(max_length=120, verbose_name='公司名称')
    type = models.IntegerField(verbose_name='公司类型, 1:无限责任公司、2:有限责任公司、3:两合公司、4:股份有限公司、5:股份两合公司')
    uniform_social_credit_code = models.CharField(max_length=120, verbose_name='统一社会信用代码')
    city = models.ForeignKey(City, null=True, verbose_name='城市', db_constraint=False, on_delete=models.DO_NOTHING,
                             related_name='enterprise_city_temp')
    city_name = models.CharField(max_length=50, default='')
    province = models.ForeignKey(City, null=True, db_constraint=False, on_delete=models.DO_NOTHING,
                                 related_name='enterprise_province_temp')
    province_name = models.CharField(max_length=50, default='')
    address = models.CharField(max_length=150, verbose_name='地址')
    legal_representative = models.CharField(max_length=100, verbose_name='法定代表人')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True, null=True)
    update_time = models.DateTimeField(auto_now=True)


class EnterPriseFileTemp(models.Model):
    user = models.ForeignKey(Users, db_constraint=False, on_delete=models.DO_NOTHING,
                             related_name='enterprise_file_temp')
    file = models.FileField(upload_to='users/enterprise', null=True, verbose_name='文件')
    type = models.IntegerField()  # 1：business_license 营业执照
    is_authentication = models.IntegerField(default=0, verbose_name='''0表示未审核''')
    is_deleted = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)