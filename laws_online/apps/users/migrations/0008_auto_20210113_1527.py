# Generated by Django 3.1.5 on 2021-01-13 15:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('conversation', '0011_auto_20210112_1154'),
        ('users', '0007_auto_20210113_1521'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='users',
            name='conversation_type',
        ),
        migrations.AddField(
            model_name='lawyer',
            name='conversation_type',
            field=models.ManyToManyField(blank=True, related_name='conversation_type_user', to='conversation.ConversationType'),
        ),
    ]
