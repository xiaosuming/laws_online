# Generated by Django 3.1.5 on 2021-03-15 01:37

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0046_auto_20210315_0056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='enterpriseusertemp',
            name='user',
            field=models.OneToOneField(db_constraint=False, on_delete=django.db.models.deletion.DO_NOTHING, related_name='enterprise_user_temp', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='lawyerauthenticationinfotemp',
            name='user',
            field=models.OneToOneField(db_constraint=False, on_delete=django.db.models.deletion.DO_NOTHING, related_name='lawyer_temp', to=settings.AUTH_USER_MODEL),
        ),
    ]
