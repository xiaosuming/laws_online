from django_filters import rest_framework as filters
from .models import Users, LawyerCase
from laws_online.apps.city.models import City
from django.db.models import Q

class LawyerSearch(filters.FilterSet):
    city = filters.CharFilter(method='city_filter')
    conversation_type = filters.CharFilter(method='conversation_type_filter')
    # is_online = filters.BooleanFilter(field_name='is_online')
    name = filters.CharFilter(field_name='nickname', lookup_expr='icontains')
    # is_authentication = filters.CharFilter(method='is_authentication_filter')

    def city_filter(self, queryset, name, value):
        city = City.objects.filter(ad_code=value).first()
        if city is None:
            return queryset
        return queryset.filter(lawyer_user__city=city)

    def conversation_type_filter(self, queryset, name, value):
        try:
            int(value)
        except ValueError:
            return queryset
        return queryset.filter(lawyer_type_user__conversation_type__pk=value)

    class Meta:
        model = Users
        fields = ['conversation_type', 'is_online', 'city', 'name']


class LawyerCaseSearch(filters.FilterSet):
    user = filters.CharFilter(field_name='user')

    class Meta:
        model = LawyerCase
        fields = ['user']