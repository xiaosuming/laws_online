from django.shortcuts import render
from django.db import transaction
from rest_framework.viewsets import ModelViewSet, GenericViewSet, ReadOnlyModelViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, ListModelMixin
from rest_framework import status
from laws_online.libs.utils.tools import Response
from rest_framework.decorators import action
from django_filters.rest_framework import DjangoFilterBackend
from .models import UserComplaint, LawyerEvaluation, PlatformSuggestionComplaint, OrdinaryUserEvaluation
from .serializers import (LawyerComplaintSerializer, LawyerEvaluationSerializer, PlatformSuggestionComplaintSerializer,
                          OrdinaryUserEvaluationSerializer)
from .filters import LawyerEvaluationSearch
from laws_online.apps.users.utils import post_im_message_in_group, check_im_online
from django.core.cache import cache
from rest_framework.permissions import AllowAny
from django_redis import get_redis_connection
from laws_online.libs.utils.tools import get_snowflake
import random


# Create your views here.


class UserComplaintView(GenericViewSet, ListModelMixin, RetrieveModelMixin, CreateModelMixin):
    serializer_class = LawyerComplaintSerializer
    queryset = UserComplaint.objects.filter(is_deleted=False).order_by('create_time')

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        '''
            创建投诉
        '''
        with cache.lock(f'user_complaint_{request.user.pk}', timeout=10, blocking_timeout=10):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            conversation = serializer.validated_data['conversation']
            if UserComplaint.objects.filter(is_deleted=False, from_user=request.user, conversation=conversation,
                                            status=0).exists():
                return Response('当前订单存在未解决的投诉', status=status.HTTP_400_BAD_REQUEST)

            data = {**serializer.validated_data, **{'user': request.user}}
            instance = serializer.create(data)
            # type 100为个人投诉， 200为律师投诉， 300为平台投诉， 400为平台建议
            im_data = {
                'type': 100 if request.user.role in [100, 200] else 200,
                'title': '投诉',
                'instance': instance.pk,
            }
            group_id = 'UserComplaint' if request.user.role in [100, 200] else 'LawyerComplaint'
            post_im_message_in_group(group_id, im_data, 'custom')
            return Response({'id': instance.pk}, status=status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        '''
        投诉详情
        '''
        instance = self.get_object()
        if request.user != instance.from_user:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        '''
        用户的投诉列表
        '''
        queryset = self.filter_queryset(self.get_queryset().filter(from_user=request.user))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class LawyerEvaluationView(GenericViewSet, ListModelMixin, RetrieveModelMixin, CreateModelMixin):
    serializer_class = LawyerEvaluationSerializer
    queryset = LawyerEvaluation.objects.filter(is_deleted=False).order_by('create_time')
    filter_backends = [DjangoFilterBackend]
    filter_class = LawyerEvaluationSearch

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        '''
        创建评价
        '''
        with cache.lock(f'lawyer_evaluation_{request.user.pk}', timeout=10, blocking_timeout=10):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            if LawyerEvaluation.objects.filter(conversation=serializer.validated_data['conversation'],
                                               is_deleted=False).exists():
                return Response('该咨询已评价，请勿重复评价', status=status.HTTP_400_BAD_REQUEST)

            data = {**serializer.validated_data, **{'user': request.user}}
            instance = serializer.create(data)
            return Response({'id': instance.pk}, status=status.HTTP_201_CREATED)



    def retrieve(self, request, *args, **kwargs):
        '''
        评价详情，无需Authorization请使用/api/v1/complaint/evaluation/lawyer/{id}/for_visit
        '''
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        '''
        用户的评价列表, 可路径传参?lawyer={id},lawyer为律师id，无需Authorization请使用
        /api/v1/complaint/evaluation/lawyer/list/for_visit
        '''
        if request.query_params.get('lawyer'):
            queryset = self.filter_queryset(self.get_queryset().filter())
        else:
            queryset = self.filter_queryset(self.get_queryset().filter(user=request.user))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['GET'], detail=False, url_path='list/for_visit', permission_classes=[AllowAny])
    def get_evaluation_list_for_visit(self, request, *args, **kwargs):
        '''
            用户获取用户的评价列表, 可路径传参?lawyer={id},lawyer为律师id
        '''
        return self.list(request, *args, **kwargs)

    @action(methods=['GET'], detail=True, url_path='for_visit', permission_classes=[AllowAny])
    def get_evaluation_for_visit(self, request, pk, *args, **kwargs):
        '''
            游客获取单个评价
        '''
        kwargs['pk'] = pk
        return self.retrieve(request, *args, **kwargs)


class OrdinaryUserEvaluationView(GenericViewSet, RetrieveModelMixin, CreateModelMixin):
    serializer_class = OrdinaryUserEvaluationSerializer
    queryset = OrdinaryUserEvaluation.objects.filter(is_deleted=False).order_by('create_time')
    filter_backends = [DjangoFilterBackend]
    # filter_class = LawyerEvaluationSearch

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        '''
        创建评价
        '''
        with cache.lock(f'ordinary_user_evaluation_{request.user.pk}', timeout=10, blocking_timeout=10):
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            if OrdinaryUserEvaluation.objects.filter(conversation=serializer.validated_data['conversation'],
                                                     is_deleted=False).exists():
                return Response('该咨询已评价，请勿重复评价', status=status.HTTP_400_BAD_REQUEST)

            data = {**serializer.validated_data}
            instance = serializer.create(data)
            return Response({'id': instance.pk}, status=status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        '''
        律师评价用户的详情
        '''
        instance = self.get_object()
        if request.user != instance.lawyer:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    # def list(self, request, *args, **kwargs):
    #     '''
    #     用户的评价列表, 可路径传参?lawyer={id},lawyer为律师id，无需Authorization请使用
    #     /api/v1/complaint/evaluation/lawyer/list/for_visit
    #     '''
    #     if request.query_params.get('lawyer'):
    #         queryset = self.filter_queryset(self.get_queryset().filter())
    #     else:
    #         queryset = self.filter_queryset(self.get_queryset().filter(user=request.user))
    #
    #     page = self.paginate_queryset(queryset)
    #     if page is not None:
    #         serializer = self.get_serializer(page, many=True)
    #         return self.get_paginated_response(serializer.data)
    #
    #     serializer = self.get_serializer(queryset, many=True)
    #     return Response(serializer.data)
    #
    # @action(methods=['GET'], detail=False, url_path='list/for_visit', permission_classes=[AllowAny])
    # def get_evaluation_list_for_visit(self, request, *args, **kwargs):
    #     '''
    #         用户获取用户的评价列表, 可路径传参?lawyer={id},lawyer为律师id
    #     '''
    #     return self.list(request, *args, **kwargs)

    # @action(methods=['GET'], detail=True, url_path='for_visit', permission_classes=[AllowAny])
    # def get_evaluation_for_visit(self, request, pk, *args, **kwargs):
    #     '''
    #         游客获取单个评价
    #     '''
    #     kwargs['pk'] = pk
    #     return self.retrieve(request, *args, **kwargs)



class PlatformSuggestionComplaintView(GenericViewSet, ListModelMixin, RetrieveModelMixin, CreateModelMixin):
    serializer_class = PlatformSuggestionComplaintSerializer
    queryset = PlatformSuggestionComplaint.objects.filter(is_deleted=False).order_by('id')

    def create(self, request, *args, **kwargs):
        '''
        创建平台投诉或评价
        '''

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = {**serializer.validated_data, **{'user': request.user}}
        data['status'] = 0 if data['type'] == 1 else -1
        data['unique_id'] = get_snowflake()
        instance = serializer.create(data)
        # type 100为个人投诉， 200为律师投诉， 300为平台投诉， 400为平台建议
        im_data = {
            'type': 300 if instance.type == 1 else 400,
            'title': '投诉' if instance.type == 1 else '建议',
            'instance': instance.pk,
        }
        post_im_message_in_group('PlatformComplaintSuggestion', im_data, 'custom')
        return Response({'id': instance.pk}, status=status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        '''
        平台投诉评价详情
        '''
        instance = self.get_object()
        if request.user != instance.user:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        '''
        平台投诉评价列表
        '''
        queryset = self.filter_queryset(self.get_queryset().filter(user=request.user))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)

        return Response(serializer.data)

    @action(methods=['GET'], detail=False, url_path='get_customer_uid')
    def get_customer_uid(self, request, *args, **kwargs):
        '''
            随机获取客服uid
        '''
        con = get_redis_connection('default')
        customers = con.lrange('manager', 0, -1)
        # customers = check_im_online(customers)
        customers_len = len(customers)
        if customers_len <= 0:
            return Response('暂无客服服务', status=status.HTTP_400_BAD_REQUEST)
        customer = customers[random.randint(0, customers_len - 1)].decode()
        return Response({'customer_uid': customer})

    # def destroy(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     instance.is_deteled = True
    #     return Response('', status=status.HTTP_204_NO_CONTENT)

