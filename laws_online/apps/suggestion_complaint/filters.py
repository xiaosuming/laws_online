from django_filters import rest_framework as filters
from .models import LawyerEvaluation


class LawyerEvaluationSearch(filters.FilterSet):

    class Meta:
        model = LawyerEvaluation
        fields = ('lawyer', )