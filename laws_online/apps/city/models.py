from django.db import models

# Create your models here.

class City(models.Model):
    city_code = models.IntegerField(null=True, db_index=True)
    ad_code = models.IntegerField(db_index=True)
    name = models.CharField(max_length=60, db_index=True)
    longitude = models.CharField(max_length=60)
    latitude = models.CharField(max_length=60)
    level = models.CharField(max_length=50)
    parent = models.ForeignKey('self', null=True, on_delete=models.DO_NOTHING, related_name='city_parent')