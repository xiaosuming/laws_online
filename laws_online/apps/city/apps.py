from django.apps import AppConfig


class CityConfig(AppConfig):
    name = 'laws_online.apps.city'
