from rest_framework import serializers
from rest_framework.serializers import ValidationError
from .models import City


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ('id', 'name', 'city_code', 'ad_code', 'parent', 'level')