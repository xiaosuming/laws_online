from django.shortcuts import render
from .serializes import CitySerializer
from .models import City
from laws_online.libs.utils.tools import Response
from laws_online.libs.utils.models import Convert
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, ListModelMixin
from rest_framework import status
from rest_framework.permissions import AllowAny


# Create your views here.
class CityView(GenericViewSet, ListModelMixin):
    serializer_class = CitySerializer
    queryset = City.objects.filter()

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset()).order_by(Convert('name', 'gbk').asc())

        page = self.paginate_queryset(queryset)
        page_index = request.query_params.get('page')
        if page is not None and page_index:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        return Response({'result': serializer.data})
