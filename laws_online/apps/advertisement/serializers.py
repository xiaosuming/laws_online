from rest_framework import serializers
from .models import AdvertisementPicture
# from rest_framework.serializers import ValidationError
from laws_online.apps.conversation.models import Conversation
from laws_online.libs.utils.custom_serializers import CustomFileField, CustomImageField

class AdvertisementPictureSerializer(serializers.ModelSerializer):
    # picture = CustomImageField()

    class Meta:
        model = AdvertisementPicture
        fields = ('picture', 'link_url', 'type')


class CarouselNewsSerializer(serializers.ModelSerializer):
    type_name = serializers.CharField(source='type.name')
    nickname = serializers.CharField(source='ordinary_user.nickname')

    class Meta:
        model = Conversation
        fields = ('nickname', 'type_name', 'create_time')