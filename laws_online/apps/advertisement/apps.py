from django.apps import AppConfig


class AdvertisementConfig(AppConfig):
    name = 'laws_online.apps.advertisement'
