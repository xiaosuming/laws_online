from django.shortcuts import render
from rest_framework.viewsets import ReadOnlyModelViewSet
# from rest_framework.mixins import RetrieveModelMixin
from rest_framework import status
from .serializers import AdvertisementPictureSerializer, CarouselNewsSerializer
from laws_online.apps.conversation.models import Conversation
from .models import AdvertisementPicture
from rest_framework.permissions import AllowAny
from laws_online.libs.utils.tools import Response
from rest_framework.decorators import action
from django.db.models import Q


# Create your views here.

class AdvertisementPictureView(ReadOnlyModelViewSet):
    serializer_class = AdvertisementPictureSerializer
    queryset = AdvertisementPicture.objects.filter(is_deleted=False)
    permission_classes = [AllowAny]

    def retrieve(self, request, *args, **kwargs):
        '''
        获取单个轮播图信息
        '''
        return super().retrieve(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        '''
        获取轮播图列表
        '''
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response({'result': serializer.data})

    @action(methods=['GET'], detail=False, url_path='carousel_news', permission_classes=[AllowAny],
            serializer_class=CarouselNewsSerializer)
    def carousel_news(self, request, *args, **kwargs):

        enterprise = self.get_serializer(
            Conversation.objects.filter(ordinary_user__role=200, is_deleted=False).filter(~Q(status=2)).order_by(
                '-create_time')[0:5], many=True)

        ordinary = self.get_serializer(
            Conversation.objects.filter(ordinary_user__role=100, is_deleted=False).filter(~Q(status=2)).order_by(
                '-create_time')[0:5], many=True)

        for i in range(len(enterprise.data)):
            nickname = enterprise.data[i]['nickname']
            enterprise.data[i]['nickname'] = f"{nickname[0] if nickname else '*'}**"

        for i in range(len(ordinary.data)):
            nickname = ordinary.data[i]['nickname']
            ordinary.data[i]['nickname'] = f"{nickname[0] if nickname else '*'}**"

        return Response({'enterprise': enterprise.data, 'ordinary': ordinary.data})


def user_privacy(request):
    return render(request, 'public/userPrivacy.html')