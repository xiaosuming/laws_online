from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'picture', views.AdvertisementPictureView, 'advertisement_picture')
# router.register(r'family', views.GetFamilyInfoView, 'family_info')
# router.register(r'platform', views.GetPlatformDataView, 'platform')
# router.register(r'family_information', views.FamilyInformationView, 'family_public_information')


urlpatterns = [
    path('advertisement/', include((router.urls, 'laws_online'), namespace='advertisement')),
    path('public/user_privacy', views.user_privacy)
]
