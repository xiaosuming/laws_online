from django.db import models

# Create your models here.
class AdvertisementPicture(models.Model):
    picture = models.ImageField(upload_to='advertisement')
    link_url = models.URLField(default='', blank=True)
    link_uid = models.CharField(max_length=100, default='')
    type = models.IntegerField(default=1)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    create_by = models.IntegerField(null=True)
    update_by = models.IntegerField(null=True)
    is_deleted = models.BooleanField(default=0)