from django.contrib import admin

# Register your models here.
from .models import AdvertisementPicture

# Register your models here.

@admin.register(AdvertisementPicture)
class AdvertisementPictureAdmin(admin.ModelAdmin):
    # isdelete.short_description = '是否删除'

    list_display = ['id','type', 'picture', 'is_deleted']
    #默认可编辑字段
    list_display_links = list_display