from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet, GenericViewSet, ReadOnlyModelViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, ListModelMixin
from rest_framework import status
from .models import Text, TextTemplateFields, UserText
from laws_online.apps.conversation.models import ConversationType, Conversation
from .serializers import (TextSerializer, TextTemplateFieldsSerializer, TextTemplateFieldsValidateSerializer,
                          UserTextSerializer, UserTextTemplateSerializer, DocxFileSerializer, NoneSerializer)
from rest_framework.permissions import AllowAny, IsAuthenticated
from laws_online.libs.utils.tools import Response
from django.db import transaction
from rest_framework.decorators import action
from django.http import StreamingHttpResponse, HttpResponse, FileResponse
from .utils import get_template_name_and_create_docx_file, replace_template_in_docx, save_docx
from docx import Document
from laws_online.settings.base import MEDIA_URL
import os

# Create your views here.


class TextView(GenericViewSet, RetrieveModelMixin, ListModelMixin, CreateModelMixin):
    queryset = Text.objects.filter(is_deleted=False).order_by('id')
    serializer_class = TextSerializer

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        '''
        app端无需使用
        '''
        # if request.user.role != 1000:
        #     return Response('1', status=status.HTTP_400_BAD_REQUEST)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.create(serializer.validated_data)
        get_template_name_and_create_docx_file(instance)
        return Response('提交成功', status=status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        '''
        获取文本详情 {"id": "(int)id", "name": "(str)名称", "file": "(str)文件url"}
        '''
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        '''
            获取文本列表 {"id": "(int)id", "name": "(str)名称", "file": "(str)文件url"}
        '''
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        page_index = request.query_params.get('page')
        if page is not None and page_index:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'result': serializer.data})


    @action(methods=['GET'], detail=True, url_path='file_form_name', serializer_class=TextTemplateFieldsSerializer)
    def get_file_form_name(self, request, pk, *args, **kwargs):
        '''
        获取文本的表单内容 {"id": "(int)id", "form_name": "(str)给用户展示的字段"},
        '''
        instance = self.get_object()
        serializer = self.get_serializer(TextTemplateFields.objects.filter(is_deleted=False, text=instance), many=True)
        return Response({'result': serializer.data})

    @action(methods=['PUT'], detail=True, url_path='download_or_collect_file',
            serializer_class=UserTextTemplateSerializer)
    def get_download_file(self, request, pk, *args, **kwargs):
        '''
            下载文本, 参数为数组形式，[{'id':'1', 'value':'1'}, {'id':'2', 'value':'2'}], key为text_values
            有两个路径参数file_type:默认为pdf，接受值为pdf,docx，select: 默认为download，接受值为download(下载),collect(收藏)
        '''
        print(request.data)
        file_type = request.query_params.get('file_type', 'pdf')
        select = request.query_params.get('select', 'download')

        file_type = file_type if file_type in ['docx', 'pdf'] else 'pdf'
        select = select if select in ['collect', 'download'] else 'download'

        if select == 'collect' and UserText.objects.filter(is_deleted=False, user=request.user).count() >= 20:
            return Response('收藏的合同不能超过20个', status=status.HTTP_400_BAD_REQUEST)

        instance = self.get_object()
        text_fields = TextTemplateFields.objects.filter(is_deleted=False, text=instance)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        id_dict = {i.get('id'): i.get('value') for i in serializer.validated_data['text_values']}
        print(94, id_dict)
        exchange_data = []
        # print(name_list)
        for i in text_fields:
            # if id_dict.get(str(i.id)) is None:
            #     return Response('参数错误', status=status.HTTP_400_BAD_REQUEST)
            form_value = id_dict.get(str(i.id))
            exchange_data.append({'name': f'id_{i.id}', 'value': form_value if form_value else '      '})
        print(102, exchange_data)
        doc = replace_template_in_docx(instance, exchange_data)
        if select == 'download':
            if file_type == 'docx':
                response = HttpResponse(
                    content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                response['Content-Disposition'] = f"attachment; filename= {instance.name}"
                doc.save(response)
                return response
            else:
                pdf_path = save_docx(request.user, doc, False)
                print(99, pdf_path)
                pdf = open(pdf_path, 'rb')
                response = FileResponse(pdf)
                response['content_type']='application/pdf'
                response['Content-Disposition'] = f"attachment; filename= {instance.name}"
                return response
        else:
            user_text = save_docx(request.user, doc)
            name = serializer.validated_data.get('name')
            user_text.name = name if name else instance.name
            user_text.save()
            return Response({'id': user_text.pk}, status=status.HTTP_201_CREATED)

    @action(methods=['POST'], detail=False, url_path='turn_docx_to_pdf', serializer_class=DocxFileSerializer)
    def turn_docx_to_pdf(self, request, *args, **kwargs):
        '''
        转换成pdf
        '''
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        print(request.build_absolute_uri('asdfda'))
        docx = Document(serializer.validated_data['docx'])
        path = save_docx(request.user, docx, False, 'tmp_turn_file')
        absolute_path, file_name = os.path.split(path)
        path = os.path.join('tmp_turn_file', file_name)
        url = f"{request.META['wsgi.url_scheme']}://{request.get_host()}{MEDIA_URL}{path}"
        return Response({'url': url}, status=status.HTTP_201_CREATED)

    @action(methods=['POST'], detail=True, url_path='collect_public_text', serializer_class=NoneSerializer)
    def collect_public_text(self, request, *args, **kwargs):
        '''
            保存公版合同
        '''
        instance = self.get_object()
        if UserText.objects.filter(text=instance, user=request.user, is_deleted=False).exists():
            return Response('用户已收藏', status=status.HTTP_400_BAD_REQUEST)
        user_text = UserText.objects.create(text=instance, user=request.user, name=instance.name,
                                            file=instance.file, pdf_file=instance.pdf_file)
        return Response({'id': user_text.pk}, status=status.HTTP_201_CREATED)



class UserTextView(GenericViewSet, RetrieveModelMixin, ListModelMixin, DestroyModelMixin):
    queryset = UserText.objects.filter(is_deleted=False).order_by('-id')
    serializer_class = UserTextSerializer

    def list(self, request, *args, **kwargs):
        '''
        获取我保存的合同
        '''
        queryset = self.filter_queryset(self.get_queryset().filter(user=request.user))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        '''
        获取单个合同
        '''
        instance = self.get_object()
        if request.user != instance.user:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        '''
        删除我保存的合同
        '''
        instance = self.get_object()
        if request.user != instance.user:
            return Response('权限错误', status=status.HTTP_403_FORBIDDEN)
        instance.is_deleted = True
        instance.save()
        return Response('', status=status.HTTP_200_OK)


# class TextTemplateFieldsView(GenericViewSet, RetrieveModelMixin):
#     serializer_class = TextTemplateFieldsSerializer
#     queryset = TextTemplateFields.objects.filter(is_deleted=False)
#
#     def retrieve(self, request, *args, **kwargs):
