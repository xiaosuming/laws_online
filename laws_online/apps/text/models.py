from django.db import models

# Create your models here.

class Text(models.Model):
    name = models.CharField(max_length=150, verbose_name='文本名称')
    file = models.FileField(verbose_name='文本', upload_to='text/file')
    origin_file = models.FileField(verbose_name='原始文本', upload_to='text/origin')
    pdf_file = models.FileField(verbose_name='pdf格式文本', upload_to='text/pdf', null=True)
    template_file = models.FileField(verbose_name='编辑时使用的文本', upload_to='text/template', null=True)
    type = models.IntegerField(verbose_name='文本类型, 1表示docx', default=1)
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    is_deleted = models.BooleanField(default=False)


class UserText(models.Model):
    text = models.ForeignKey(Text, on_delete=models.DO_NOTHING, db_constraint=False, null=True,
                                      related_name='text_user', verbose_name='文本')
    user = models.ForeignKey('users.Users', on_delete=models.DO_NOTHING, db_constraint=False,
                                      related_name='user_text', verbose_name='用户')
    name = models.CharField(max_length=150, verbose_name='文本名称', default='')
    file = models.FileField(verbose_name='文件', upload_to='text/user')
    pdf_file = models.FileField(verbose_name='pdf文件', upload_to='text/user')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    is_deleted = models.BooleanField(default=False)


class TextTemplateFields(models.Model):
    text = models.ForeignKey(Text, on_delete=models.DO_NOTHING, db_constraint=False,
                             related_name='text_template', verbose_name='文本')
    name = models.CharField(max_length=150, verbose_name='文本字段名称', default='')
    form_name = models.CharField(max_length=150, verbose_name='文本字段描述')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    is_deleted = models.BooleanField(default=False)