# -*- coding: utf-8 -*-
"""
linux platform word to pdf
"""
import subprocess
import os

try:
    from comtypes import client
    # import pythoncom
except ImportError:
    client = None
    try:
        from win32com.client import constants, gencache
    except ImportError:
        constants = None
        gencache = None


def doc2pdf_linux(docPath, pdfPath):
    """
        convert a doc/docx document to pdf format (linux only, requires libreoffice)
            :param doc: path to document
                """
    cmd = 'libreoffice --headless --convert-to pdf'.split() + [docPath] + ['--outdir'] + [pdfPath]
    print(cmd)
    p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    p.wait(timeout=30)
    stdout, stderr = p.communicate()
    # print(29, stdout,30, stderr)
    # if stderr:
    #     raise subprocess.SubprocessError(stderr)


def doc2pdf(docPath, pdfPath):
    """
            convert a doc/docx document to pdf format
                    :param doc: path to document
                            """
    docPathTrue = os.path.abspath(docPath)  # bugfix - searching files in windows/system32
    if client is None:  # 判断环境，linux环境这里肯定为None
        return doc2pdf_linux(docPathTrue, pdfPath)
    try:
        wdFormatPDF = 17
        word = client.CreateObject('Word.Application')
        doc = word.Documents.Open(docPathTrue)
        doc.SaveAs(pdfPath, FileFormat=wdFormatPDF)
        doc.Close()
        word.Quit()
    except:
        pass
    # word = gencache.EnsureDispatch('Word.Application')
    # doc = word.Documents.Open(docPathTrue, ReadOnly=1)
    # doc.ExportAsFixedFormat(pdfPath,
    #                         constants.wdExportFormatPDF,
    #                         Item=constants.wdExportDocumentWithMarkup,
    #                         CreateBookmarks=constants.wdExportCreateHeadingBookmarks)
    # word.Quit(constants.wdDoNotSaveChanges)



if __name__ == '__main__':
    # wordpath = r'/home/bonus18/test/tests/he.docx'
    # pdfpath = r'/home/bonus18/test/tests/'
    wordpath = r'D:\PycharmProjects\laws_online\laws_online\upload\text\origin\he.docx'
    pdfpath = r'D:\PycharmProjects\laws_online\laws_online\upload\tmp\he.pdf'
    doc2pdf(wordpath, pdfpath)
