from docx import Document
from docx.oxml.ns import qn
from laws_online.settings.base import MEDIA_ROOT, BASE_DIR
from laws_online.apps.text.models import TextTemplateFields, Text, UserText
# import comtypes.client
import os
import re
from django.template import Context, Template
from laws_online.libs.utils.tools import get_snowflake
from .word_to_pdf import doc2pdf

def get_template_name_and_create_docx_file(instance):
    doc = Document(os.path.join(MEDIA_ROOT, instance.origin_file.path))
    fields = []
    paragraphs = doc.paragraphs
    for paragraph in paragraphs:
        runs = paragraph.runs
        for run in runs:
            # doc.styles['Normal'].font.name = run.font.name
            # doc.styles['Normal'].font.size = run.font.size
            # doc.styles['Normal'].font.bold = run.font.bold
            # doc.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), run.font.name)
            # try:
            #     doc.styles['Normal'].element.rPr.rFonts.set(qn('w:eastAsia'), run.font.name)
            # except Exception as e:
            #     print(e)

            text = run.text
            # change_text = re.sub(r'{{(.*?)}}', '【 】', text)
            change_text = text.replace('{{', '【').replace('}}', '】')
            # print(text)
            # print(change_text)
            # print(str(text) != str(change_text))
            # print(text != change_text)
            if text != change_text:
                field = [s.strip().replace('-', '_').replace(' ', '_') for s in re.findall(r'{{(.*?)}}', text)]
                for i in field:
                    if not i:
                        return f'{i}参数格式错误'
                    # if i[0] in [str(n) for n in range(0, 10)]:
                    #     return f'{i}不能以数字开头'

                fields = fields + field
                run.text = change_text

    file_upload_to = Text._meta.get_field('file').upload_to.replace('/', '\\')
    origin_file_upload_to = Text._meta.get_field('origin_file').upload_to.replace('/', '\\')
    file_path = rf'{instance.origin_file.name}'.replace(origin_file_upload_to,
                                                        file_upload_to).replace('.docx', '_change.docx')

    # print(origin_file_upload_to)
    # print(file_upload_to)
    # print(file_path)
    # print(rf'{instance.origin_file.path}')
    # origin_absolute_path = os.path.join(MEDIA_ROOT, )
    file_absolute_path = os.path.join(MEDIA_ROOT, file_path)
    # file_pdf_path = file_path.replace('.docx', '.pdf')
    # file_pdf_absolute_path = os.path.join(MEDIA_ROOT, file_pdf_path)
    # file_absolute_path = file_absolute_path
    doc.save(file_absolute_path)
    # convert_docx_to_pdf(file_absolute_path, file_pdf_absolute_path)
    print(file_path)
    print(file_absolute_path)
    instance.file = file_path
    # instance.pdf_file = file_pdf_path
    instance.save()
    fields_models = [TextTemplateFields(form_name=i, text=instance) for i in fields]
    t = TextTemplateFields.objects.bulk_create(fields_models)
    print(t)
    return True



def replace_template_in_docx(instance, data):
    context = Context({i['name']: i['value'] for i in data})
    print(75, context)
    doc = Document(os.path.join(MEDIA_ROOT, instance.template_file.path))
    paragraphs = doc.paragraphs
    for paragraph in paragraphs:
        text = paragraph.text
        change_text_template = Template(text)
        change_text = change_text_template.render(context)

        if text != change_text:
            paragraph.text = change_text
        # runs = paragraph.runs
        # for run in runs:
        #     # doc.styles['Normal'].font.name = run.font.name
        #     # doc.styles['Normal'].font.size = run.font.size
        #     # doc.styles['Normal'].font.bold = run.font.bold
        #     # doc.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), run.font.name)
        #     # try:
        #     #     doc.styles['Normal'].element.rPr.rFonts.set(qn('w:eastAsia'), run.font.name)
        #     # except Exception as e:
        #     #     print(e)
        #
        #     text = run.text
        #     change_text_template = Template(text)
        #     change_text = change_text_template.render(context)
        #     # print(text)
        #     # print(change_text)
        #     # print(str(text) != str(change_text))
        #     # print(text != change_text)
        #     if text != change_text:
        #         run.text = change_text

    return doc


def save_docx(user, docx, need_save_to_local=True, save_path='tmp'):
    snowflake_id = get_snowflake()
    if need_save_to_local:
        file_upload_to = UserText._meta.get_field('file').upload_to
        print(105,file_upload_to)
        pdf_file_upload_to = UserText._meta.get_field('pdf_file').upload_to
        file_path = os.path.join(file_upload_to, f'text_{snowflake_id}.docx')
        pdf_file_path = os.path.join(pdf_file_upload_to, f'text_{snowflake_id}.pdf')
        file_absolute_path = os.path.join(MEDIA_ROOT, file_path)
        print(file_absolute_path)
        pdf_file_absolute_path = os.path.join(MEDIA_ROOT, pdf_file_upload_to)
        docx.save(file_absolute_path)  # 保存docx
        doc2pdf(file_absolute_path, pdf_file_absolute_path)  # 保存pdf
        user_text = UserText.objects.create(file=file_path, pdf_file=pdf_file_path, user=user)
        return user_text
    else:
        file_path = os.path.join(MEDIA_ROOT, save_path)
        file_docx_path = os.path.join(file_path, f'text_{user.pk}_{snowflake_id}.docx')
        docx.save(file_docx_path)
        file_pdf_path = os.path.join(file_path, f'text_{user.pk}_{snowflake_id}.pdf')
        # print(file_pdf_path)
        # print(file_pdf_path.replace(str(BASE_DIR), ''))
        doc2pdf(file_docx_path, file_path)  # 保存pdf
        return file_pdf_path

# def convert_docx_to_pdf(infile, outfile):
#     wdFormatPDF = 17
#     word = comtypes.client.CreateObject('Word.Application')
#     doc = word.Documents.Open(infile)
#     doc.SaveAs(outfile, FileFormat=wdFormatPDF)
#     doc.Close()
#     word.Quit()