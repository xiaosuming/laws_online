from django.apps import AppConfig


class ConversationConfig(AppConfig):
    name = 'laws_online.apps.conversation'
