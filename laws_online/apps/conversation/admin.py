from django.contrib import admin

# Register your models here.
from .models import ConversationType

# Register your models here.

@admin.register(ConversationType)
class UsersAdmin(admin.ModelAdmin):
    # isdelete.short_description = '是否删除'

    list_display = ['id','name', 'is_deleted', 'picture']
    #默认可编辑字段
    list_display_links = list_display

# @admin.register(Supplier_driver_ship)
# class UsersAdmin(admin.ModelAdmin):
#     # isdelete.short_description = '是否删除'
#
#     list_display = ['supplier','driver']
#     #默认可编辑字段
#     list_display_links = list_display
#
# @admin.register(Vehicle_driver_ship)
# class UsersAdmin(admin.ModelAdmin):
#     # isdelete.short_description = '是否删除'
#
#     list_display = ['driver','vehicle']
#     #默认可编辑字段
#     list_display_links = list_display
