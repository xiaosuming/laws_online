from rest_framework.permissions import BasePermission
from laws_online.apps.users.models import Users
from django.contrib.auth.models import AnonymousUser


class OrdinaryUserPermission(BasePermission):
    message = '用户权限错误'

    def has_permission(self, request, view):
        if isinstance(request.user, AnonymousUser) or not request.user:
            return False

        if request.user.role not in [100, 200, 1000]:
            return False
        return True


class LawyerUserPermission(BasePermission):
    message = '用户权限错误'

    def has_permission(self, request, view):
        if isinstance(request.user, AnonymousUser) or not request.user:
            return False

        if request.user.role not in [300, 1000]:
            return False

        return True


class IsAuthenticatedByManager(BasePermission):
    message = '用户未认证'

    def has_permission(self, request, view):
        if isinstance(request.user, AnonymousUser) or not request.user:
            return False

        if request.user.status != 0:
            return False

        if request.user.is_authentication == 0:
            return False

        return True


class IsAuthenticatedAndNoBanned(BasePermission):
    message = '用户已被禁用'

    def has_permission(self, request, view):
        if isinstance(request.user, AnonymousUser) or not request.user:
            return False

        # if request.user.status != 0:
        #     return False

        return True