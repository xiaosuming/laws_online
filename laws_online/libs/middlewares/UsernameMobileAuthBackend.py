import re
from laws_online.apps.users.models import Users
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.hashers import check_password
from django.db.models import Q

def get_user_by_account(account, password, role):
    """
    添加通过手机号查询用户的方法
    """
    try:
        # if re.match(r'^1[356789]\d{9}$', str(account)):  # account 是手机号
        #     user = Users.objects.get(phone=account, is_deleted=False)
        # else:
        #     user = Users.objects.get(uid=account ,is_deleted=False)
        # user = Users.objects.get(Q(phone=account) | Q(uid=account), Q(is_deleted=False), Q(role=role))
        user = Users.objects.get(Q(phone=account), Q(is_deleted=False), Q(role=role))
        print(user)
        # from django.db import connection
        # for i in connection.queries:
        #     print(i)
        # print(user)
    except Users.DoesNotExist as e:
        print(e)
        return None
    else:
        if not check_password(password, user.password):
            return None
        return user


class UserByUsernameMobile(ModelBackend):
    """添加支持手机号登录"""

    def authenticate(self, request, uid=None, password=None, verification_code=None, role=100, *args, **kwargs):
        if uid and password:
            user = get_user_by_account(uid, password, role)  # username 可能是用户名也可能是手机号
            return user
        if uid and verification_code:
            return Users.objects.filter(Q(phone=uid), Q(is_deleted=False), Q(role=role)).first()


