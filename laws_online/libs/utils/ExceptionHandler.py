from django.core.exceptions import PermissionDenied
from rest_framework.serializers import ValidationError
from rest_framework.exceptions import AuthenticationFailed, ErrorDetail, NotFound, NotAuthenticated, PermissionDenied
from laws_online.libs.utils.tools import Response
from rest_framework.views import exception_handler
# import  rest_framework.utils.serializer_helpers.ReturnDict


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)
    print(type(exc))
    if response is not None:

        if response.status_code == 500:
            print(type(exc))
            response.data['code'] = response.status_code
            response.data['msg'] = '服务器异常'
            response.data['data'] = {}

        elif isinstance(exc, ValidationError):
            print(response.data)
            try:
                msg = ','.join([','.join([f'{j}' for j in v]) for k, v in response.data.items()])
            except Exception:
                code = response.status_code
                msg = ','.join(response.data)
                response.data = {
                    'code': code,
                    'msg': msg,
                    'data': ''
                }
            else:
                response.data['code'] = response.status_code
                response.data['msg'] = msg
                response.data['data'] = {}
                print(response.data)
            # response_keys = list(response.data.keys())
            # print(response_keys)
            # for i in response_keys:
            #     response.data.pop(i)
            # print(str(response.data['non_field_errors'][0]))


        elif isinstance(exc, NotFound):
            msg = ','.join([','.join([f'{k}: {j}' for j in v]) for k, v in response.data.items()])
            # response_keys = list(response.data.keys())
            # for i in response_keys:
            #     response.data.pop(i)
            response.data['code'] = response.status_code
            response.data['msg'] = msg
            response.data['data'] = {}
            print(response.data)

        elif isinstance(exc, AuthenticationFailed):
            print(response.data)
            msg = ','.join([v for k, v in response.data.items()])
            # response_keys = list(response.data.keys())
            # for i in response_keys:
            #     response.data.pop(i)
            response.data['code'] = response.status_code
            response.data['msg'] = msg
            response.data['data'] = {}
            print(response.data)

        elif isinstance(exc, NotAuthenticated):
            print(response.data)
            msg = ','.join([f'{v}' for k, v in response.data.items()])
            # response_keys = list(response.data.keys())
            # for i in response_keys:
            #     response.data.pop(i)
            response.data['code'] = response.status_code
            response.data['msg'] = msg
            response.data['data'] = {}
            print(response.data)

        elif isinstance(exc, PermissionDenied):
            print(response.data)
            msg = ','.join([f'{v}' for k, v in response.data.items()])
            # response_keys = list(response.data.keys())
            # for i in response_keys:
            #     response.data.pop(i)
            response.data['code'] = response.status_code
            response.data['msg'] = msg
            response.data['data'] = {}
            print(response.data)

            
        # response.data['message'] =response.data['detail']    #增加message这个key
        # response.data['message'] ='方法不对'    #增加message这个key

    return response



# def custom_exception_handler(exc, context):
#
#     data = {
#         'code': 500,
#         'msg': "",
#         'data': {}
#     }
#
#     if isinstance()