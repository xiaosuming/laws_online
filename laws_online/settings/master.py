from .base import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'laws_online',
        'USER': 'root',
        'PASSWORD': 'zfzx2021',
        'HOST': '172.17.0.1',
        'PORT': '3306',
        'OPTIONS': {
            "init_command": "SET foreign_key_checks = 0;",
            'charset': 'utf8mb4'
        },
    },
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://172.17.0.1:6379/10",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PASSWORD": 'zfzx2021'
        },
        'TIMEOUT': 7200
    },
}

DEBUG = False

# SECURITY安全设置 - 支持http时建议开启
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
SECURE_SSL_REDIRECT = True # 将所有非SSL请求永久重定向到SSL
SESSION_COOKIE_SECURE = True # 仅通过https传输cookie
CSRF_COOKIE_SECURE = True # 仅通过https传输cookie
SECURE_HSTS_INCLUDE_SUBDOMAINS = True # 严格要求使用https协议传输
SECURE_HSTS_PRELOAD = True # HSTS为
SECURE_HSTS_SECONDS = 60
SECURE_CONTENT_TYPE_NOSNIFF = True # 防止浏览器猜测资产的内容类型


REDISPSWORD = 'zfzx2021'
REDISHOST = '172.17.0.1'
REDISPORT = 6379


# CELERY 配置
BROKER_BACKEND = 'redis'
BROKER_URL = f'redis://:{REDISPSWORD}@{REDISHOST}:{REDISPORT}/7'  # Broker配置，使用Redis作为消息中间件


CELERY_ENABLE_UTC = True

CELERY_TIMEZONE = TIME_ZONE

CELERY_RESULT_BACKEND = f'redis://:{REDISPSWORD}@{REDISHOST}:{REDISPORT}/8'  # BACKEND配置，这里使用redis

CELERY_TASK_SERIALIZER = 'pickle'

CELERY_RESULT_SERIALIZER = 'pickle'  # 结果序列化方案

CELERY_ACCEPT_CONTENT = ['pickle', 'json']

CELERYD_MAX_TASKS_PER_CHILD = 10 # 每个worker执行多少次任务后死亡，防止内存泄漏用的