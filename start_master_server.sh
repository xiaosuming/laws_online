user=root
ip=101.132.24.189
text=$*
ssh $user@$ip 'docker stop laws_online && docker rm laws_online'
ssh $user@$ip "docker run --restart=always --name laws_online -d -e DJANGO_CONFIGURATION=master -v /var/www/laws_online/upload:/app/laws_online/laws_online/upload -v /var/www/laws_online/static:/app/laws_online/laws_online/static -v /var/www/laws_online/logs:/app/laws_online/laws_online/logs -v /var/www/laws_online/fonts:/usr/share/fonts -p 8200:8000 registry.cn-shanghai.aliyuncs.com/xinhuodev/laws_online:$text"